<?php

/**
 * Child Theme's textdomain.
 *
 * Theme Translations can be overwritten in the /languages/ directory.
 */
function grve_child_theme_setup() {
	load_child_theme_textdomain( 'osmosis', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'grve_child_theme_setup' );






/**
 * Below command removes Categories and tags from the product page
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );






/**
 * Adds login/logout in top header
 */
add_action('grve_header_top_bar_right_last_item', 'grve_child_theme_top_bar_login_form', 1);
function grve_child_theme_top_bar_login_form() {

	ob_start();

	if ( is_user_logged_in() ){
		$grve_login_classes = array( 'grve-admin-form', 'grve-language' );

		$grve_login_class_string = implode( ' ', $grve_login_classes );	
		$current_user = wp_get_current_user();
		
	?>
		<li class=" grve-topbar-item">
			<ul class="<?php echo $grve_login_class_string; ?>">
				<li>
					<p class="grve-admin-form-label"><?php echo __( '', 'osmosis' ) . $current_user->user_login; ?></p>
					<ul>
						<li>
							<a href="/my-account/" title="'<?php echo __( 'My Account', osmosis ); ?>"><?php echo __( 'My Account', osmosis ); ?></a>
							<a href="/my-account/edit-account/" title="'<?php echo __( 'Change Password', osmosis ); ?>"><?php echo __( 'Change Password', osmosis ); ?></a>						
							<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="<?php echo __( 'Logout', osmosis ); ?>"><?php echo __( 'Logout', osmosis ); ?></a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	<?php
	} else {
		$grve_login_classes = array( 'grve-admin-form', 'grve-language' );
		$grve_login_class_string = implode( ' ', $grve_login_classes );			
	?>
		<li class=" grve-topbar-item">
			<ul class="<?php echo $grve_login_class_string; ?>">
				<li>
					<p class="grve-admin-form-label"><?php echo __( 'Log in ', osmosis ); ?></p>
					<ul>
						<li class="grve-login-admin-form">
							<?php echo wp_login_form( array( 'echo' => false, 'remember' => true ) );?>
						</li>
						<li>
							<?php wp_register( '', '' ); ?><a href="<?php echo wp_lostpassword_url( get_permalink() );?>" title="<?php echo __( 'Lost Password', osmosis ); ?>"><?php echo __( 'Lost Password', osmosis ); ?></a>
						</li>
					</ul>
				</li>
			</ul>
		</li>	
	<?php
	}

	echo ob_get_clean();
}






/**
 * Hides the 'Free!' price notice
 */
add_filter( 'woocommerce_variable_free_price_html',  'hide_free_price_notice' );
 
add_filter( 'woocommerce_free_price_html',           'hide_free_price_notice' );
 
add_filter( 'woocommerce_variation_free_price_html', 'hide_free_price_notice' );
 





/**
 * Hides the 'Free!' price notice
 */
function hide_free_price_notice( $price ) {
 
  return '';
}

add_filter( 'woocommerce_variation_free_price_html', 'woocommerce_template_loop_add_to_cart' );






/**
 * Hides the 'Add to Cart' from 'CERTIFICATE OF MEMBER' when member is not logged in
 */
function woocommerce_template_single_add_to_cart() {

	if(is_user_logged_in()){
		woocommerce_simple_add_to_cart();
	}else{
		if(get_the_title()!='Certificate of Member'){
			woocommerce_simple_add_to_cart();
		}else{
			echo '<span style="padding: 12px 17px; line-height: 32px; background-color: #000000; font-family: Open Sans; font-weight: 700; font-style: normal; font-size: 11px ; text-transform: uppercase; color: white !important;"><a href="/my-account/edit-account/" style="color: white !important;">Please login to add to cart!<a></span>';
		}

	}
}






/**
 * Creates a shortcode that displays "Recent Order" only!!
 */
function shortcode_my_orders( $atts ) {
    extract( shortcode_atts( array(
        'order_count' => -1
    ), $atts ) );

    ob_start();
    wc_get_template( 'myaccount/my-orders.php', array(
        'current_user'  => get_user_by( 'id', get_current_user_id() ),
        'order_count'   => $order_count
    ) );
    return ob_get_clean();
}
add_shortcode('my_orders', 'shortcode_my_orders');






/**
 * Add login/logout link to naviagation menu!!
 */
function add_login_out_item_to_menu( $items, $args ){

	//change theme location with your them location name
	if( is_admin() ||  $args->theme_location != 'osmosis' )
		return $items; 

	$redirect = ( is_home() ) ? false : get_permalink();
	if( is_user_logged_in( ) )
		$link = '<a href="' . wp_logout_url( $redirect ) . '" title="' .  __( 'Logout' ) .'">' . __( 'Logout' ) . '</a>';
	else  $link = '<a href="' . wp_login_url( $redirect  ) . '" title="' .  __( 'Login' ) .'">' . __( 'Login' ) . '</a>';

	return $items.= '<li id="log-in-out-link" class="menu-item menu-type-link">'. $link . '</li>';
}add_filter( 'wp_nav_menu_items', 'add_login_out_item_to_menu', 50, 2 );






/**
 * Disable "product reviews" by deafault!!
 */
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {

 unset($tabs['reviews']);

 return $tabs;
}






/**
 * Hide "PERSONAL OPTION" from users/members!!
 */
function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
add_action('admin_head','hide_personal_options');






/**
 * Hide "ABOUT THE USER" from users/members!!
 */
if (! function_exists('remove_plain_bio') ){
    function remove_plain_bio($buffer) {
        $titles = array('#<h3>About Yourself</h3>#','#<h3>About the user</h3>#');
        $buffer=preg_replace($titles,'<h3>Password</h3>',$buffer,1);
        $biotable='#<h3>Password</h3>.+?<table.+?/tr>#s';
        $buffer=preg_replace($biotable,'<h3>Password</h3> <table class="form-table">',$buffer,1);
        return $buffer;
    }

    function profile_admin_buffer_start() { ob_start("remove_plain_bio"); }

    function profile_admin_buffer_end() { ob_end_flush(); }
}
add_action('admin_head', 'profile_admin_buffer_start');
add_action('admin_footer', 'profile_admin_buffer_end');






/**
 * Hide "WEBSITE" from "Contact Info" in users/members!!
 */
function remove_website_row_wpse_94963_css()
{
    echo '<style>tr.user-url-wrap{ display: none; }</style>';
}
add_action( 'admin_head-user-edit.php', 'remove_website_row_wpse_94963_css' );
add_action( 'admin_head-profile.php',   'remove_website_row_wpse_94963_css' );






/**
 * Disable "Sort By.." in categories/products!!
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

?>
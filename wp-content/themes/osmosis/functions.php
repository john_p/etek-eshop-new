<?php

/*
*	Main theme functions and definitions
*
* 	@version	1.0
* 	@author		Greatives Team
* 	@URI		http://greatives.eu
*/

/**
 * Theme Definitions
 * Please leave these settings unchanged
 */

define( 'GRVE_THEME_SHORT_NAME', 'osmosis' );
define( 'GRVE_THEME_TRANSLATE', 'osmosis' );
define( 'GRVE_THEME_NAME', 'Osmosis' );
define( 'GRVE_THEME_MAJOR_VERSION', 2 );
define( 'GRVE_THEME_MINOR_VERSION', 5 );
define( 'GRVE_THEME_HOTFIX_VERSION', 1 );
define( 'GRVE_REDUX_CUSTOM_PANEL', false);

/**
 * Set up the content width value based on the theme's design.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1080;
}

/**
 * Include Global helper files
 */
require_once 'includes/grve-global.php';
/**
 * Include WooCommerce helper files
 */
require_once 'includes/grve-woocommerce-functions.php';
/**
 * Include Events Calendar helper files
 */
require_once 'includes/grve-events-calendar-functions.php';
/**
 * Include bbPress helper files
 */
require_once 'includes/grve-bbpress-functions.php';

/**
 * Register Plugins Libraries
 */
if ( is_admin() ) {
	require_once 'includes/plugins/tgm-plugin-activation/register-plugins.php';
	require_once 'includes/plugins/envato-wordpress-toolkit-library/class-envato-wordpress-theme-upgrader.php';
}

/**
 * ReduxFramework
 */

require_once 'includes/admin/grve-redux-extension-loader.php';

if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/includes/framework/framework.php' ) ) {
    require_once 'includes/framework/framework.php';
}


if ( !isset( $redux_demo ) ) {
	require_once 'includes/admin/grve-redux-framework-config.php';
}

function grve_remove_redux_demo_link() {
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
    }
}
add_action('init', 'grve_remove_redux_demo_link');

/**
 * Visual Composer Extentions
 */
if ( class_exists( 'WPBakeryVisualComposerAbstract' ) ) {

	function grve_add_vc_extentions() {
		require_once 'vc_extend/grve-shortcodes-vc-helper.php';
		require_once 'vc_extend/grve-shortcodes-vc-remove.php';
		require_once 'vc_extend/grve-shortcodes-vc-add.php';
	}
	add_action( 'init', 'grve_add_vc_extentions', 5 );

}

/**
 * Include admin helper files
 */
require_once 'includes/admin/grve-admin-functions.php';
require_once 'includes/admin/grve-admin-custom-sidebars.php';
require_once 'includes/admin/grve-admin-media-functions.php';
require_once 'includes/admin/grve-admin-feature-functions.php';

require_once 'includes/admin/grve-update-functions.php';
require_once 'includes/admin/grve-meta-functions.php';
require_once 'includes/admin/grve-page-meta.php';
require_once 'includes/admin/grve-post-meta.php';

require_once 'includes/admin/grve-portfolio-post-type.php';
require_once 'includes/admin/grve-portfolio-meta.php';
require_once 'includes/admin/grve-testimonial-post-type.php';
require_once 'includes/admin/grve-testimonial-meta.php';
require_once 'includes/grve-wp-gallery.php';

/**
 * Include Dynamic css
 */
require_once 'includes/grve-dynamic-css-loader.php';

/**
 * Include helper files
 */
require_once 'includes/grve-excerpt.php';
require_once 'includes/grve-vce-functions.php';
require_once 'includes/grve-header-functions.php';
require_once 'includes/grve-feature-functions.php';
require_once 'includes/grve-layout-functions.php';
require_once 'includes/grve-blog-functions.php';
require_once 'includes/grve-media-functions.php';
require_once 'includes/grve-portfolio-functions.php';
require_once 'includes/grve-footer-functions.php';

/**
 * Include Theme Widgets
 */
require_once 'includes/widgets/grve-widget-social.php';
require_once 'includes/widgets/grve-widget-latest-posts.php';
require_once 'includes/widgets/grve-widget-latest-comments.php';
require_once 'includes/widgets/grve-widget-latest-combo.php';
require_once 'includes/widgets/grve-widget-latest-portfolio.php';
require_once 'includes/widgets/grve-widget-contact-info.php';
require_once 'includes/widgets/grve-widget-instagram-feed.php';

//Add shortcodes to widget text
add_filter( 'widget_text' , 'do_shortcode' );

add_action( "after_switch_theme", "grve_theme_activate" );
add_action( 'after_setup_theme', 'grve_theme_setup' );
add_action( 'widgets_init', 'grve_register_sidebars' );

/**
 * Theme activation function
 * Used whe activating the theme
 */
function grve_theme_activate() {

	$grve_version = array (
		"major" => GRVE_THEME_MAJOR_VERSION,
		"minor" => GRVE_THEME_MINOR_VERSION,
		"hotfix" => GRVE_THEME_HOTFIX_VERSION,
	);
	update_option( 'grve_theme_' . GRVE_THEME_SHORT_NAME . '_version', $grve_version );
	flush_rewrite_rules();
}

/**
 * Theme setup function
 * Theme translations and support
 */
function grve_theme_setup() {

	load_theme_textdomain( 'osmosis', get_template_directory() . '/languages' );

	add_theme_support( 'menus' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails', array( 'post', 'portfolio' ) );
	add_theme_support( 'post-formats', array( 'gallery', 'link', 'quote', 'video', 'audio' ) );
	add_theme_support( 'title-tag' );

	add_image_size( 'grve-image-extrasmall-square', 80, 80, true );
	add_image_size( 'grve-image-large-rect-horizontal', 1170, 658, true );
	add_image_size( 'grve-image-small-square', 560, 560, true );
	add_image_size( 'grve-image-small-rect-horizontal', 560, 315, true );
	add_image_size( 'grve-image-medium-rect-vertical', 560, 1120, true );
	add_image_size( 'grve-image-medium-rect-horizontal', 1120, 560, true );
	add_image_size( 'grve-image-medium-square', 1120, 1120, true );
	add_image_size( 'grve-image-fullscreen', 1920, 1920, false );

	register_nav_menus(
		array(
			'grve_header_nav' => __( 'Header Menu', 'osmosis' ),
			'grve_footer_nav' => __( 'Footer Menu', 'osmosis' ),
		)
	);

}

/**
 * Navigation Menus
 */
function grve_get_header_nav() {

	$grve_main_menu = '';

	if ( 'default' == grve_option( 'menu_header_integration', 'default' ) ) {

		if ( is_singular() ) {
			if ( 'yes' == grve_post_meta( 'grve_disable_menu' ) ) {
				return 'disabled';
			} else {
				$grve_main_menu	= grve_post_meta( 'grve_main_navigation_menu' );
			}
		}
		if( grve_woocommerce_enabled() && is_shop() && !is_search()  ) {
			if ( 'yes' == grve_post_meta_shop( 'grve_disable_menu' ) ) {
				return 'disabled';
			} else {
				$grve_main_menu	= grve_post_meta_shop( 'grve_main_navigation_menu' );
			}
		}
	} else {
		$grve_main_menu = 'disabled';
	}

	$grve_main_menu = apply_filters( 'grve_custom_header_nav', $grve_main_menu );

	return $grve_main_menu;
}

function grve_header_nav( $grve_main_menu = '') {

	if ( empty( $grve_main_menu ) ) {
		wp_nav_menu(
			array(
				'menu_class' => 'grve-menu', /* menu class */
				'theme_location' => 'grve_header_nav', /* where in the theme it's assigned */
				'container' => false,
				'fallback_cb' => 'grve_fallback_menu',
			)
		);
	} else {
		//Custom Alternative Menu
		wp_nav_menu(
			array(
				'menu_class' => 'grve-menu', /* menu class */
				'menu' => $grve_main_menu, /* menu name */
				'container' => false,
				'fallback_cb' => 'grve_fallback_menu',
			)
		);
	}
}

function grve_footer_nav() {

	wp_nav_menu(
		array(
			'theme_location' => 'grve_footer_nav',
			'container' => false, /* no container */
			'depth' => '1',
		)
	);

}

/**
 * Sidebars & Widgetized Areas
 */
function grve_register_sidebars() {

	register_sidebar( array(
		'id' => 'grve-default-sidebar',
		'name' => __( 'Main Sidebar', 'osmosis' ),
		'description' => __( 'Main Sidebar Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));

	register_sidebar( array(
		'id' => 'grve-single-portfolio-sidebar',
		'name' => __( 'Single Portfolio', 'osmosis' ),
		'description' => __( 'Single Portfolio Sidebar Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));

	if ( grve_woocommerce_enabled() ) {

		register_sidebar( array(
			'id' => 'grve-woocommerce-sidebar-shop',
			'name' => __( 'Shop Overview Page', 'osmosis' ),
			'description' => __( 'Shop Overview Widget Area', 'osmosis' ),
			'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="grve-widget-title">',
			'after_title' => '</h5>',
		));
		register_sidebar( array(
			'id' => 'grve-woocommerce-sidebar-product',
			'name' => __( 'Shop Product Pages', 'osmosis' ),
			'description' => __( 'Shop Product Widget Area', 'osmosis' ),
			'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="grve-widget-title">',
			'after_title' => '</h5>',
		));
	}

	register_sidebar( array(
		'id' => 'grve-footer-1-sidebar',
		'name' => __( 'Footer 1', 'osmosis' ),
		'description' => __( 'Footer 1 Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-2-sidebar',
		'name' => __( 'Footer 2', 'osmosis' ),
		'description' => __( 'Footer 2 Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-3-sidebar',
		'name' => __( 'Footer 3', 'osmosis' ),
		'description' => __( 'Footer 3 Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-4-sidebar',
		'name' => __( 'Footer 4', 'osmosis' ),
		'description' => __( 'Footer 4 Widget Area', 'osmosis' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));

	$grve_custom_sidebars = get_option( 'grve-osmosis-custom-sidebars' );
	if ( ! empty( $grve_custom_sidebars ) ) {
		foreach ( $grve_custom_sidebars as $grve_custom_sidebar ) {
			register_sidebar( array(
				'id' => $grve_custom_sidebar['id'],
				'name' => __( 'Custom Sidebar', 'osmosis' ) . ': ' . $grve_custom_sidebar['name'],
				'description' => '',
				'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h5 class="grve-widget-title">',
				'after_title' => '</h5>',
			));
		}
	}

}

/**
 * Custom Search Form
 */
function grve_wpsearch( $form ) {
	$new_custom_id = uniqid( 'grve_search_' );
	$form =  '<form role="search" class="grve-search" method="get" action="' . home_url( '/' ) . '" >';
	$form .= '  <button type="submit" class="grve-search-btn"><i class="grve-icon-search"></i></button>';
	$form .= '  <input type="text" class="grve-search-textfield" id="' . esc_attr( $new_custom_id ) . '" value="' . get_search_query() . '" name="s" placeholder="' . esc_html__( 'Search for ...', 'osmosis' ) . '" />';
    $form .= '  <input type="hidden" name="post_type" value="product" />';
    $form .= '</form>';
	return $form;
}
add_filter( 'get_search_form', 'grve_wpsearch' );

/**
 * Enqueue scripts and styles for the front end.
 */
function grve_frontend_scripts() {

	$template_dir_uri = get_template_directory_uri();
	$child_theme_dir_uri = get_stylesheet_directory_uri();

	wp_register_style( 'grve-style', $child_theme_dir_uri."/style.css", array(), '2.5.1', 'all' );
	wp_enqueue_style( 'grve-awsome-fonts', $template_dir_uri . '/css/font-awesome.min.css', array(), '4.3.0' );


	wp_enqueue_style( 'grve-basic', $template_dir_uri . '/css/basic.css', array(), '2.5.1' );
	wp_enqueue_style( 'grve-grid', $template_dir_uri . '/css/grid.css', array(), '2.5.1' );
	wp_enqueue_style( 'grve-theme-style', $template_dir_uri . '/css/theme-style.css', array(), '2.5.1' );
	wp_enqueue_style( 'grve-elements', $template_dir_uri . '/css/elements.css', array(), '2.5.1' );

	if ( grve_woocommerce_enabled() ) {
		wp_enqueue_style( 'grve-woocommerce-layout', $template_dir_uri . '/css/woocommerce-layout.css', array(), '2.5.1', 'all' );
		wp_enqueue_style( 'grve-woocommerce-smallscreen', $template_dir_uri . '/css/woocommerce-smallscreen.css', array( 'grve-woocommerce-layout' ), '2.5.1', 'only screen and (max-width: 959px)' );
		wp_enqueue_style( 'grve-woocommerce-extrasmallscreen', $template_dir_uri . '/css/woocommerce-extrasmallscreen.css', array( 'grve-woocommerce-layout' ), '2.5.1', 'only screen and (max-width: 767px)' );
		wp_enqueue_style( 'grve-woocommerce-general', $template_dir_uri . '/css/woocommerce.css', array(), '2.5.1', 'all' );
	}

	if ( grve_events_calendar_enabled() ) {
		wp_enqueue_style( 'grve-events-calendar', $template_dir_uri . '/css/events-calendar.css', array(), '2.5.1', 'all' );
	}

	if ( $child_theme_dir_uri !=  $template_dir_uri ) {
		wp_enqueue_style( 'grve-style');
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_style( 'grve-responsive', $template_dir_uri . '/css/responsive.css', array(), '2.5.1' );

	wp_register_script( 'grve-googleapi-script', '//maps.googleapis.com/maps/api/js?v=3&sensor=false', NULL, NULL, true );
	wp_register_script( 'grve-maps-script', $template_dir_uri . '/js/maps.js', array( 'jquery', 'grve-googleapi-script' ), '2.5.1', true );
	$grve_maps_data = array(
		'hue_enabled' => grve_option( 'gmap_hue_enabled', '0' ) ,
		'hue' => grve_option( 'gmap_hue', '#ffffff' ) ,
		'saturation' => grve_option( 'gmap_saturation', '0' ) ,
		'lightness' => grve_option( 'gmap_hue', '0' ) ,
		'gamma' => grve_option( 'gmap_gamma', '0.1' ) ,
	);
	wp_localize_script( 'grve-maps-script', 'grve_maps_data', $grve_maps_data );
	wp_enqueue_script( 'grve-modernizr-script', $template_dir_uri . '/js/modernizr.custom.js', array( 'jquery' ), '2.8.3', false );
	$smooth_scroll = grve_option( 'smooth_scroll_enabled', '1' );
	if ( grve_browser_webkit_check() && '1' == $smooth_scroll ) {
		wp_enqueue_script( 'grve-smoothscrolling-script', $template_dir_uri . '/js/smoothscrolling.js', array( 'jquery' ), '1.2.1', true );
	}

	wp_enqueue_script( 'grve-plugins', $template_dir_uri . '/js/plugins.js', array( 'jquery' ), '2.5.1', true );
	$grve_plugins_data = array(
		'retina_support' => grve_option( 'retina_support', 'default' ),
	);
	wp_localize_script( 'grve-plugins', 'grve_plugins_data', $grve_plugins_data );

	wp_enqueue_script( 'grve-smartresize-script', $template_dir_uri . '/js/smartresize.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'grve-isotope-script', $template_dir_uri . '/js/isotope.pkgd.min.js', array( 'jquery' ), '2.0.0', true );
	wp_enqueue_script( 'grve-packery-mode-script', $template_dir_uri . '/js/packery-mode.pkgd.min.js', array( 'jquery' ), '0.1.0', true );
	wp_enqueue_script( 'grve-main-script', $template_dir_uri . '/js/main.js', array( 'jquery' ), '2.5.1', true );
	$grve_row_stellar_auto = apply_filters( 'grve_row_stellar_auto', '1' );
	$grve_main_data = array(
		'siteurl' => $template_dir_uri ,
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'grve_wp_gallery_popup' => grve_option( 'wp_gallery_popup', '0' ),
		'grve_row_stellar_auto' => $grve_row_stellar_auto,
	);
	wp_localize_script( 'grve-main-script', 'grve_main_data', $grve_main_data );

}
add_action( 'wp_enqueue_scripts', 'grve_frontend_scripts' );

/**
 * Pagination functions
 */
function grve_paginate_links() {
?>
	<div class="grve-pagination">
	<?php
		global $wp_query;
		$big = 999999999;
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'prev_text'    => "<i class='grve-icon-nav-left'></i>",
			'next_text'    => "<i class='grve-icon-nav-right'></i>",
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'add_args' => false,
		) );
	?>
	</div>
<?php
}

function grve_wp_link_pages() {
?>
	<?php
		$args = array(
			'before'           => '<p>',
			'after'            => '</p>',
			'link_before'      => '',
			'link_after'       => '',
			'next_or_number'   => 'number',
			'nextpagelink'     => "<i class='grve-icon-nav-right'></i>",
			'previouspagelink' => "<i class='grve-icon-nav-left'></i>",
			'pagelink'         => '%',
			'echo'             => 1
		);
	?>
	<div class="grve-pagination">
	<?php wp_link_pages( $args ); ?>
	</div>
<?php
}

function grve_pagination( $pages = '', $range = 2 ) {
	 $showitems = ( $range * 2 )+1;

	 global $paged;
	 if ( empty( $paged ) ) $paged = 1;

	 if ( $pages == '' ) {
		 global $wp_query;
		 $pages = $wp_query->max_num_pages;
		 if(!$pages){
			 $pages = 1;
		 }
	 }

	if ( 1 != $pages ) {
		$pagination =  "<div class='grve-pagination'>";
		$pagination .=  "<ul>";

		if ( $paged > 2 && $paged > $range+1 && $showitems < $pages ) {
			$pagination .= "<li><a class='grve-pagination-nav' href='" . get_pagenum_link( 1 ) . "' data-page='1'><i class='grve-icon-nav-double-left'></i></a></li>";
		}
		if( $paged > 1 && $showitems < $pages ){
			$this_page = $paged - 1;
			$pagination .= "<li><a class='grve-pagination-nav' href='" . get_pagenum_link( $paged - 1 ) . "' data-page='" . $this_page . "'><i class='grve-icon-nav-left'></i></a></li>";
		}

		for ( $i = 1; $i <= $pages; $i++ ) {
			if ( 1 != $pages &&( !( $i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ) ){
				$pagination .= ( $paged == $i )? "<li><a class='current' href='#'>" . $i . "</a></li>":"<li><a href='" . get_pagenum_link( $i )."' data-page='" . $i . "'>" . $i . "</a></li>";
			}
		}

		if ( $paged < $pages && $showitems < $pages ) {
			$this_page = $paged + 1;
			$pagination .= "<li><a class='grve-pagination-nav' href='" . get_pagenum_link( $paged + 1 ) . "' data-page='" . $this_page . "'><i class='grve-icon-nav-right'></i></a></li>";
		}
        if ( $paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages ){
			$pagination .= "<li><a class='grve-pagination-nav' href='" . get_pagenum_link( $pages ) . "' data-page='" . $pages . "'><i class='grve-icon-nav-double-right'></i></a></li>";
		}
		$pagination .=  "</ul>";
		$pagination .=  "</div>";
		echo $pagination;
     }
}

/**
 * Comments
 */
function grve_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	?>
	<li class="grve-comment-item">
		<!-- Comment -->
		<article id="comment-<?php comment_ID(); ?>"  <?php comment_class(); ?>>
			<?php echo get_avatar( $comment, 50 ); ?>
			<div class="grve-comment-content">

				<h6 class="grve-author">
					<a href="<?php comment_author_url( $comment->comment_ID ); ?>"><?php comment_author(); ?></a>
				</h6>
				<div class="grve-comment-date">
					<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( ' %1$s ' . __( 'at', 'osmosis' ) . ' %2$s', get_comment_date(),  get_comment_time() ); ?></a>
				</div>
				<?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __( 'REPLY', 'osmosis' ) ) ) ); ?>
				<?php edit_comment_link( __( 'EDIT', 'osmosis' ), '  ', '' ); ?>

				<?php if ( $comment->comment_approved == '0' ) : ?>
					<p><?php _e( 'Your comment is awaiting moderation.', 'osmosis' ); ?></p>
				<?php endif; ?>
				<?php comment_text(); ?>
			</div>
		</article>

	<!-- </li> is added by WordPress automatically -->
<?php
}

/**
 * Avatar additional Class
 */
function grve_add_gravatar_class( $class ) {
    $class = str_replace( "class='avatar", "class='avatar grve-circle", $class );
    return $class;
}
add_filter('get_avatar','grve_add_gravatar_class');

/**
 * Navigation links for prev/next in comments
 */
function grve_replace_reply_link_class( $output ) {
	$class = 'grve-btn grve-primary grve-btn-extrasmall grve-comment-reply';
	return preg_replace( '/comment-reply-link/', 'comment-reply-link ' . $class, $output, 1 );
}
add_filter('comment_reply_link', 'grve_replace_reply_link_class');

function grve_replace_edit_link_class( $output ) {
	$class = 'grve-btn grve-primary grve-btn-extrasmall grve-comment-edit';
	return preg_replace( '/comment-edit-link/', 'comment-edit-link ' . $class, $output, 1 );
}
add_filter('edit_comment_link', 'grve_replace_edit_link_class');

/**
 * Main Navigation FallBack Menu
 */
function grve_fallback_menu(){

	echo '<ul class="grve-menu">';
	wp_list_pages('title_li=&sort_column=menu_order');
	echo '</ul>';
}

/**
 * Title Render Fallback before WordPress 4.1
 */
 if ( ! function_exists( '_wp_render_title_tag' ) ) {
	function grve_theme_render_title() {
?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php
	}
	add_action( 'wp_head', 'grve_theme_render_title' );
}

/**
 * Theme identifier function
 * Used to get theme information
 */
function grve_theme_osmosis_info() {

	$grve_info = array (
		"major_version" => GRVE_THEME_MAJOR_VERSION,
		"minor_version" => GRVE_THEME_MINOR_VERSION,
		"hotfix_version" => GRVE_THEME_HOTFIX_VERSION,
		"short_name" => GRVE_THEME_SHORT_NAME,
	);

	return $grve_info;
}


/*function custom_user_profile_fields($user){
    //var_dump(the_author_meta( 'etek', $user->ID ));
    ?>

    <table class="form-table">
        <tr>
            <th><label for="etek">ETEK No.<span class="description">(required)</span></label></th>
            <td>
                <input type="text" class="regular-text" name="etek" value="<?php echo esc_attr( get_the_author_meta( 'etek', $user->ID ) ); ?>" id="etek" /><br />

            </td>
        </tr>
    </table>
<?php
}*/
//add_action( 'show_user_profile', 'custom_user_profile_fields' );
//add_action( 'edit_user_profile', 'custom_user_profile_fields' );
//add_action( "user_new_form", "custom_user_profile_fields" );

function save_custom_user_profile_fields($user_id){
    # again do this only if you can
    if(!current_user_can('manage_options'))
        return false;

    # save my custom field
    update_usermeta($user_id, 'etek_no_', $_POST['etek_no_']);
}
add_action('user_register', 'save_custom_user_profile_fields');


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {

	/* remove shipping fields */
	unset($fields['shipping']['shipping_first_name']);
	unset($fields['shipping']['shipping_last_name']);
	unset($fields['shipping']['shipping_company']);
	unset($fields['shipping']['shipping_address_2']);
	unset($fields['shipping']['shipping_country']);
	unset($fields['shipping']['shipping_state']);
	/*end remove shipping fields */

	$fields['shipping']['shipping_address_1']['class'] = array('form-row-first', 'address-field');

	$fields['shipping']['shipping_city']['label'] = "CITY";
	$fields['shipping']['shipping_city']['placeholder'] = "CITY";

	$fields['shipping']['shipping_postcode']['label'] = "POSTAL CODE";
	$fields['shipping']['shipping_postcode']['placeholder'] = "POSTAL CODE";
	$fields['shipping']['shipping_postcode']['clear'] = false;


    $fields['shipping']['shipping_phone'] = array(
        'label'     => __('Telephone', 'woocommerce'),
        'placeholder'   => _x('Telephone', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-last'),
        'clear'     => true
    );



	/* remove billing fields */
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_state']);
	/*end remove billing fields */

	/* new billing fields */
	$new_field = array(
		'etek_no_' => array(
			'label'     => __('ETEK No.', 'woocommerce'),
			'placeholder'   => _x('ETEK No.', 'placeholder', 'woocommerce'),
			'required'  => true,
			'class'     => array('form-row-first'),
			'clear'     => false
		),

		'type' => array(
			'label'     => __('Type', 'woocommerce'),
			'type'		=> 'radio',
			'placeholder'   => _x('Type', 'placeholder', 'woocommerce'),
			'required'  => true,
			'class'     => array('form-row-last'),
			'clear'     => true,
			'options'	=> array('Individual'=>'Individual', 'Company'=>'Company'),
			'defoult'	=> 'Individual'
		)
	);

	/* end new billing fields */

	/* for checkout page */
	if(is_user_logged_in()){
		/* login user */
		global $current_user;
		get_currentuserinfo();

		$fields['billing'] = array_insert($fields['billing'], $new_field, 3);

        $defoult_address = get_user_meta($current_user->ID, 'default_address');
        //var_dump($current_user->user_email);

		//$current_user->user_firstname
		//$current_user->user_lastname
		$fields['billing']['billing_first_name']['b_value'] = $current_user->user_firstname;
		$fields['billing']['billing_last_name']['b_value'] = $current_user->user_lastname;

		//$current_user->etek_no_
		//$current_user->type

		$fields['billing']['etek_no_']['b_value'] = $current_user->etek_no_;
		$fields['billing']['type']['b_value'] = $current_user->type;

		/* hide billing fields */
		$fields['billing']['billing_first_name']['b_hide'] = true;
		$fields['billing']['billing_last_name']['b_hide'] = true;
		$fields['billing']['etek_no_']['b_hide'] = true;
		$fields['billing']['type']['b_hide'] = true;
		$fields['billing']['billing_email']['b_hide'] = true;
        $fields['billing']['billing_email']['b_value'] = $current_user->user_email;
		$fields['billing']['billing_phone']['b_hide'] = true;
		$fields['billing']['billing_country']['b_hide'] = true;
		$fields['billing']['billing_address_1']['b_hide'] = true;
		$fields['billing']['billing_city']['b_hide'] = true;
		$fields['billing']['billing_postcode']['b_hide'] = true;
		/* end hide billing fields */

	}else{
		/* logout user */
		$fields['billing'] = array_insert($fields['billing'], $new_field, 2);
		$fields['billing']['billing_city']['class'] =  array('form-row-first');

		$fields['billing']['etek_no_']['required'] =  false;
		$fields['billing']['type']['required'] =  false;
		$fields['billing']['type']['b_value'] = 'Individual';
        $fields['billing']['type']['class'][] = 'wrapp-radio-type';
        //var_dump($fields['billing']['type']);die;
	}

    return $fields;
}

// save the extra field when checkout is processed
function kia_save_extra_checkout_fields( $order_id, $posted ){
	if( isset( $posted['etek_no_'] ) ) {
		update_post_meta( $order_id, 'etek_no_', sanitize_text_field( $posted['etek_no_'] ) );
	}

	if( isset( $posted['type'] ) ) {
		update_post_meta( $order_id, 'type', sanitize_text_field( $posted['type'] ) );
	}
}
add_action( 'woocommerce_checkout_update_order_meta', 'kia_save_extra_checkout_fields', 10, 2 );

function array_insert($array,$values,$offset) {
	return array_slice($array, 0, $offset, true) + $values + array_slice($array, $offset, NULL, true);
}

add_action('pre_get_posts', 'jc_woo_search_pre_get_posts');
function jc_woo_search_pre_get_posts($q){

    if ( is_search() ) {
        add_filter( 'posts_join', 'jc_search_post_join' );
        add_filter( 'posts_where', 'jc_search_post_excerpt' );
    }
}

function jc_search_post_join($join = ''){
    if($join == ''){
        return $join;
    }

    $join .= ' INNER JOIN 92ia9r96_term_relationships ON (92ia9r96_posts.ID = 92ia9r96_term_relationships.object_id) ';

    //echo $join;
    return $join;
}

function jc_search_post_excerpt($where = ''){

    if($where == ''){
        return $where;
    }
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => 55,
        'taxonomy' => 'product_cat'
    );
    $the_query = get_categories($args);

    $sub_args = array();
    foreach($the_query as $subcat){
        $sub_args[] = $subcat->term_id;

    }

    $where = 'AND 92ia9r96_term_relationships.term_taxonomy_id IN ('.implode(',', $sub_args).')'.$where;

    return $where;
}

add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3 );
if(!function_exists('wdm_get_cart_items_from_session'))
{
	function wdm_get_cart_items_from_session($item, $values, $key)
	{
		if (array_key_exists( 'ship', $values ) )
		{
			$item['ship'] = $values['ship'];
		}
		return $item;
	}
}


add_action('woocommerce_before_cart_item_quantity_zero','remove_ship_user_custom_data_options_from_cart',1,1);
function remove_ship_user_custom_data_options_from_cart($cart_item_key)	{
	global $woocommerce;
	$cart = $woocommerce->cart->get_cart();

	foreach( $cart as $key => $values){
		if ( $values['ship'] == $cart_item_key )
			unset( $woocommerce->cart->cart_contents[ $key ] );
	}
}

add_action( 'woocommerce_before_calculate_totals', 'change_cart_item_price' );
function change_cart_item_price( $cart_object ) {
	$shiper = array();
    //echo ' before ';
    //var_dump($cart_object);
    //var_dump( WC()->session->get( 'chosen_shipping_methods') );
	if(isset($_POST['ship_method'])){
		foreach($_POST['ship_method'] as $key => $ship){
			//var_dump($ship);
			$cart_object->cart_contents[$key]['ship'] = $ship;
            $shiper[] = $ship;
		}
	}else{
        global $woocommerce;
        foreach($woocommerce->cart->get_cart() as $key => $product){
            //var_dump($product['ship']);die;
            if(!isset($product['ship']) || $product['ship'] == ''){
                $shippings = get_post_meta( $product['product_id'], 'shippings');
                if(isset($shippings[0][0]) && count($shippings[0]) == 1){
                    $cart_object->cart_contents[$key]['ship'] = $shippings[0][0];
					$shiper[] = $shippings[0][0];
                }
				
            }else{
				$shiper[] = $product['ship'];
			}
        }
    }


	//WC()->session->chosen_shipping_methods = '';
	//WC()->session->set( 'chosen_shipping_methods', array('local_delivery') );
    //var_dump($shiper)

	if(in_array('flat_rate', $shiper)){
		WC()->session->set( 'chosen_shipping_methods', array('flat_rate') ); //shipping = 2$
	}else{
		WC()->session->set( 'chosen_shipping_methods', array('local_delivery') ); //shipping = 0$
	}
    //var_dump( WC()->session->get( 'chosen_shipping_methods') );

    if(is_checkout()){
        if(empty($shiper) || in_array('0', $shiper) || count($cart_object->cart_contents) > count($shiper)){
            //var_dump($shiper);
            wc_add_notice( 'Select Shipment Method', 'error' );
            wp_redirect('cart', 301);
            exit();
        }
    }

}

add_action( 'woocommerce_before_checkout_shipping_form', 'before_shipping');



add_action('woocommerce_add_order_item_meta','add_ship_values_to_order_item_meta',1,2);
function add_ship_values_to_order_item_meta($item_id, $values){
    global $woocommerce,$wpdb;

    if(!empty($values['ship']))	{
        wc_add_order_item_meta($item_id,'ship',$values['ship']);
    }
}


add_action( 'wp_ajax_nopriv_load_woo_cart', 'load_woo_cart' );
add_action( 'wp_ajax_load_woo_cart', 'load_woo_cart' );

function load_woo_cart(){
    global $wp_session;
    global $woocommerce;

    $shiper = array();
    $result = array();

    $cart = $woocommerce->cart->get_cart();

    //$cart = new WC_Cart();
    //echo  $woocommerce->$cart->calculate_totals();die;
    //var_dump( WC()->session->get( 'chosen_shipping_methods') );

    foreach( $cart as $cart_key => $values){

        if( isset($_GET['ship_method']) ){

            foreach( $_GET['ship_method'] as $key => $ship ){
                if($cart_key == $key){
                    $cart[$key]['ship'] = $ship;
                    $shiper[] = $ship;
                }
            }

        } else {
            if( isset($values['ship']) ){
                $shiper[] = $values['ship'];
            }
        }

    }


    if(in_array('flat_rate', $shiper)){
        //echo ' flat_rate ';
        WC()->session->set( 'chosen_shipping_methods', array('flat_rate') ); //shipping = 2$
    }else{
        //echo ' local_delivery ';
        WC()->session->set( 'chosen_shipping_methods', array('local_delivery') ); //shipping = 0$
    }
    WC()->session->cart = $cart;

    $woocommerce->cart->calculate_shipping();

    $result['total'] = wc_price( $woocommerce->cart->subtotal + $woocommerce->cart->shipping_total);

    if(empty($shiper) || in_array('0', $shiper) || count($cart) > count($shiper)){
        $result['remove_err'] = false;
    }else{
        $result['remove_err'] = true;
    }

    echo json_encode( $result );

    die();
}



/* send E-mail */
// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );
// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );


function woo_add_custom_general_fields() {

    global $woocommerce, $post;

    echo '<div class="options_group">';

    woocommerce_wp_textarea_input(
        array(
            'id'          => '_multiples_emails',
            'label'       => __( 'Multiples Emails', 'woocommerce' ),
            'description' => __( 'Enter E-Mails separating by coma.', 'woocommerce' )
        )
    );

    echo '</div>';

}

function woo_add_custom_general_fields_save( $post_id ){
    $woocommerce_text_field = $_POST['_multiples_emails'];
    if( !empty( $woocommerce_text_field ) )
        update_post_meta( $post_id, '_multiples_emails', esc_attr( $woocommerce_text_field ) );
}

add_action( 'send_multiples_emails_per_product', 'multiples_emails_per_product' );
function multiples_emails_per_product( $order_id ){
    //var_dump($order_id);

    $order = new WC_Order( $order_id );
    $products = $order->get_items();


    $to_all = array();
    foreach( $products as $product ){
        //var_dump( $product['product_id'] );
        $emails = get_post_meta( (int) $product['product_id'], '_multiples_emails', true);
        $to = explode(',', $emails);
        if(!empty($to)){
            $to_all = array_merge($to, $to_all);
        }
        //$order->
    }
    $mails = WC_Emails::instance();
    if(!empty($to_all)){
        //$mails = new WC_Emails();
        //$mails = WC_Emails::instance();
        $order_sender = $mails->custom_multiple_email();
        //echo get_class($order_sender);
        $order_sender->multi_trigger($order, $to_all);

    }

    //$mails->order_processing($order_id);

    //var_dump($order_id);die;

}




?>
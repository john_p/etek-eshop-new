<?php
/*
*	Portfolio Post Type Registration
*
* 	@author		Greatives Team
* 	@URI		http://greatives.eu
*/

if ( ! class_exists( 'GRVE_Portfolio_Post_Type' ) ) {
	class GRVE_Portfolio_Post_Type {

		function __construct() {

			// Adds the portfolio post type and taxonomies
			add_action( 'init', array( &$this, 'grve_portfolio_init' ), 0 );

			// Manage Columns for portfolio overview
			add_filter( 'manage_edit-portfolio_columns',  array( &$this, 'grve_portfolio_edit_columns' ) );
			add_action( 'manage_posts_custom_column', array( &$this, 'grve_portfolio_custom_columns' ), 10, 2 );

		}

		function grve_portfolio_init() {

			$portfolio_base_slug = grve_option( 'portfolio_slug', 'portfolio' );


			$labels = array(
				'name' => _x( 'Portfolio Items', 'Portfolio General Name', 'osmosis' ),
				'singular_name' => _x( 'Portfolio Item', 'Portfolio Singular Name', 'osmosis' ),
				'add_new' => __( 'Add New', 'osmosis' ),
				'add_new_item' => __( 'Add New Portfolio Item', 'osmosis' ),
				'edit_item' => __( 'Edit Portfolio Item', 'osmosis' ),
				'new_item' => __( 'New Portfolio Item', 'osmosis' ),
				'view_item' => __( 'View Portfolio Item', 'osmosis' ),
				'search_items' => __( 'Search Portfolio Items', 'osmosis' ),
				'not_found' =>  __( 'No Portfolio Items found', 'osmosis' ),
				'not_found_in_trash' => __( 'No Portfolio Items found in Trash', 'osmosis' ),
				'parent_item_colon' => '',
			);

			$category_labels = array(
				'name' => __( 'Portfolio Categories', 'osmosis' ),
				'singular_name' => __( 'Portfolio Category', 'osmosis' ),
				'search_items' => __( 'Search Portfolio Categories', 'osmosis' ),
				'all_items' => __( 'All Portfolio Categories', 'osmosis' ),
				'parent_item' => __( 'Parent Portfolio Category', 'osmosis' ),
				'parent_item_colon' => __( 'Parent Portfolio Category:', 'osmosis' ),
				'edit_item' => __( 'Edit Portfolio Category', 'osmosis' ),
				'update_item' => __( 'Update Portfolio Category', 'osmosis' ),
				'add_new_item' => __( 'Add New Portfolio Category', 'osmosis' ),
				'new_item_name' => __( 'New Portfolio Category Name', 'osmosis' ),
			);

			$field_labels = array(
				'name' => __( 'Portfolio Fields', 'osmosis' ),
				'singular_name' => __( 'Portfolio Field', 'osmosis' ),
				'search_items' => __( 'Search Portfolio Fields', 'osmosis' ),
				'all_items' => __( 'All Portfolio Fields', 'osmosis' ),
				'parent_item' => __( 'Parent Portfolio Field', 'osmosis' ),
				'parent_item_colon' => __( 'Parent Portfolio Field:', 'osmosis' ),
				'edit_item' => __( 'Edit Portfolio Field', 'osmosis' ),
				'update_item' => __( 'Update Portfolio Field', 'osmosis' ),
				'add_new_item' => __( 'Add New Portfolio Field', 'osmosis' ),
				'new_item_name' => __( 'New Portfolio Field Name', 'osmosis' ),
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => true,
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_position' => 5,
				'menu_icon' => 'dashicons-format-gallery',
				'supports' => array( 'title', 'editor', 'author', 'excerpt', 'thumbnail', 'custom-fields', 'comments' ),
				'rewrite' => array( 'slug' => $portfolio_base_slug, 'with_front' => false ),
			);

			register_post_type( 'portfolio' , $args );

			register_taxonomy(
				'portfolio_category',
				array( 'portfolio' ),
				array(
					'hierarchical' => true,
					'label' => __( 'Portfolio Categories', 'osmosis' ),
					'labels' => $category_labels,
					'show_in_nav_menus' => false,
					'show_tagcloud' => false,
					'rewrite' => true,
				)
			);
			register_taxonomy_for_object_type( 'portfolio_category', 'portfolio' );

			register_taxonomy(
				'portfolio_field',
				array( 'portfolio' ),
				array(
					'hierarchical' => true,
					'label' => __( 'Portfolio Fields', 'osmosis' ),
					'labels' => $field_labels,
					'show_in_nav_menus' => false,
					'show_tagcloud' => false,
					'rewrite' => true,
				)
			);
			register_taxonomy_for_object_type( 'portfolio_field', 'portfolio' );

		}

		function grve_portfolio_edit_columns( $columns ) {
			$columns = array(
				'cb' => '<input type="checkbox" />',
				'portfolio_thumbnail' => __( 'Featured Image', 'osmosis' ),
				'title' => __( 'Title', 'osmosis' ),
				'author' => __( 'Author', 'osmosis' ),
				'portfolio_category' => __( 'Portfolio Categories', 'osmosis' ),
				'portfolio_field' => __( 'Portfolio Fields', 'osmosis' ),
				'date' => __( 'Date', 'osmosis' ),
			);
			return $columns;
		}

		function grve_portfolio_custom_columns( $column, $post_id ) {

			switch ( $column ) {
				case "portfolio_thumbnail":
					if ( has_post_thumbnail( $post_id ) ) {
						$thumbnail_id = get_post_thumbnail_id( $post_id );
						$attachment_src = wp_get_attachment_image_src( $thumbnail_id, array( 80, 80 ) );
						$thumb = $attachment_src[0];
					} else {
						$thumb = get_template_directory_uri() . '/includes/images/no-image.jpg';
					}
					echo '<img class="attachment-80x80" width="80" height="80" alt="portfolio image" src="' . $thumb . '">';
					break;
				case 'portfolio_category':
					echo get_the_term_list( $post_id, 'portfolio_category', '', ', ','' );
				break;
				case 'portfolio_field':
					echo get_the_term_list( $post_id, 'portfolio_field', '', ', ','' );
				break;
			}
		}

	}
	new GRVE_Portfolio_Post_Type;
}

?>
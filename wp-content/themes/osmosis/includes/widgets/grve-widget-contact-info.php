<?php
/**
 * Plugin Name: Greatives Contact Info
 * Description: A widget that displays Contact Info e.g: Address, Phone number.etc.
 * @author		Greatives Team
 * @URI			http://greatives.eu
 */

add_action( 'widgets_init', 'grve_widget_contact_info' );

function grve_widget_contact_info() {
	register_widget( 'GRVE_Widget_Contact_Info' );
}

class GRVE_Widget_Contact_Info extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'grve-contact-info',
			'description' => __( 'A widget that displays contact info', 'osmosis' ),
		);
		$control_ops = array(
			'width' => 300,
			'height' => 400,
			'id_base' => 'grve-widget-contact-info',
		);
		parent::__construct( 'grve-widget-contact-info', '(Greatives) ' . __( 'Contact Info', 'osmosis' ), $widget_ops, $control_ops );
	}

	function GRVE_Widget_Contact_Info() {
		$this->__construct();
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$name = grve_array_value( $instance, 'name' );
		$address = grve_array_value( $instance, 'address' );
		$phone = grve_array_value( $instance, 'phone' );
		$mobile = grve_array_value( $instance, 'mobile' );
		$fax = grve_array_value( $instance, 'fax' );
		$mail = grve_array_value( $instance, 'mail' );
		$web = grve_array_value( $instance, 'web' );
		$microdata = grve_array_value( $instance, 'microdata' );

		echo $before_widget;

		// Display the widget title
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$microdata_name = $microdata_address = $microdata_phone = $microdata_fax = $microdata_email = $microdata_url = '';
		if ( !empty( $microdata ) ) {

			$microdata_name = 'itemprop="name"';
			$microdata_address = 'itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"';
			$microdata_phone = 'itemprop="telephone"';
			$microdata_fax = 'itemprop="faxNumber"';
			$microdata_email = 'itemprop="email"';
			$microdata_url = 'itemprop="url"';

			echo '<div itemscope itemtype="http://schema.org/' . $microdata . '">';
		}
		?>

		<ul>

			<?php if ( ! empty( $name ) ) { ?>
			<li class="grve-user" <?php echo $microdata_name; ?>><?php echo $name; ?></li>
			<?php } ?>

			<?php if ( ! empty( $address ) ) { ?>
			<li class="grve-address" <?php echo $microdata_address; ?>><?php echo $address; ?></li>
			<?php } ?>

			<?php if ( ! empty( $phone ) ) { ?>
			<li class="grve-phone" <?php echo $microdata_phone; ?>><?php echo $phone; ?></li>
			<?php } ?>

			<?php if ( ! empty( $mobile ) ) { ?>
			<li class="grve-mobile-number" <?php echo $microdata_phone; ?>><?php echo $mobile; ?></li>
			<?php } ?>

			<?php if ( ! empty( $fax ) ) { ?>
			<li class="grve-fax" <?php echo $microdata_fax; ?>><?php echo $fax; ?></li>
			<?php } ?>

			<?php if ( ! empty( $mail ) ) { ?>
			<li class="grve-email" <?php echo $microdata_email; ?>><a href="mailto:<?php echo antispambot( $mail ); ?>"><?php echo antispambot( $mail ); ?></a></li>
			<?php } ?>

			<?php if ( ! empty( $web ) ) { ?>
			<li class="grve-web"><a href="<?php echo esc_url( $web ); ?>" target="_blank" <?php echo $microdata_url; ?>><?php echo $web; ?></a></li>
			<?php } ?>
		</ul>


		<?php

		if ( !empty( $microdata ) ) {
			echo '</div>';
		}
		echo $after_widget;
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['name'] = strip_tags( $new_instance['name'] );
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = strip_tags( $new_instance['phone'] );
		$instance['mobile'] = strip_tags( $new_instance['mobile'] );
		$instance['fax'] = strip_tags( $new_instance['fax'] );
		$instance['mail'] = strip_tags( $new_instance['mail'] );
		$instance['web'] = strip_tags( $new_instance['web'] );
		$instance['microdata'] = strip_tags( $new_instance['microdata'] );

		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array(
			'title' => '',
			'name' => '',
			'address' => '',
			'phone' => '',
			'mobile' => '',
			'fax' => '',
			'mail' => '',
			'web' => '',
			'microdata' => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>


		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php _e( 'Name:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" value="<?php echo $instance['name']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:', 'osmosis' ); ?></label>
			<textarea id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" style="width:100%;"><?php echo $instance['address']; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Phone:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mobile' ); ?>"><?php _e( 'Mobile Phone:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'mobile' ); ?>" name="<?php echo $this->get_field_name( 'mobile' ); ?>" value="<?php echo $instance['mobile']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'fax' ); ?>"><?php _e( 'Fax:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'fax' ); ?>" name="<?php echo $this->get_field_name( 'fax' ); ?>" value="<?php echo $instance['fax']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mail' ); ?>"><?php _e( 'Mail:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'mail' ); ?>" name="<?php echo $this->get_field_name( 'mail' ); ?>" value="<?php echo $instance['mail']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'web' ); ?>"><?php _e( 'Website:', 'osmosis' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'web' ); ?>" name="<?php echo $this->get_field_name( 'web' ); ?>" value="<?php echo $instance['web']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'microdata' ); ?>"><?php echo __( 'Microdata ( Schema.org ):', 'osmosis' ); ?></label>
			<select id="<?php echo $this->get_field_id('microdata'); ?>" name="<?php echo $this->get_field_name( 'microdata' ); ?>" style="width:100%;">
				<?php $microdata = $instance['microdata']; ?>
				<option value="" <?php selected( '', $microdata ); ?>><?php echo __( 'None', 'osmosis' ); ?></option>
				<option value="Person" <?php selected( 'Person', $microdata ); ?>>Person</option>
				<option value="Organization" <?php selected( 'Organization', $microdata ); ?>>Organization</option>
				<option value="Corporation" <?php selected( 'Corporation', $microdata ); ?>>Corporation</option>
				<option value="EducationalOrganization" <?php selected( 'EducationalOrganization', $microdata ); ?>>School</option>
				<option value="GovernmentOrganization" <?php selected( 'GovernmentOrganization', $microdata ); ?>>Government</option>
				<option value="LocalBusiness" <?php selected( 'LocalBusiness', $microdata ); ?>>Local Business</option>
				<option value="NGO" <?php selected( 'NGO', $microdata ); ?>>NGO</option>
				<option value="PerformingGroup" <?php selected( 'PerformingGroup', $microdata ); ?>>Performing Group</option>
				<option value="SportsTeam" <?php selected( 'SportsTeam', $microdata ); ?>>Sports Team</option>
			</select>
		</p>

	<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.

<?php

	if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
		die ( __( 'This page cannot be opened directly!', 'osmosis' ) );
	}

	if ( post_password_required() ) {
?>
		<div class="help">
			<p class="no-comments"><?php echo __( 'This post is password protected. Enter the password to view comments.', 'osmosis' ); ?></p>
		</div>
<?php
		return;
	}
?>

<?php if ( have_comments() ) : ?>

	<nav class="grve-comment-nav">
		<ul>
	  		<li><?php previous_comments_link(); ?></li>
	  		<li><?php next_comments_link(); ?></li>
	 	</ul>
	</nav>

	<!-- Comments -->
	<div id="grve-comments" class="grve-section">
		<h5 class="grve-comments-number">
			<?php comments_number( __( 'no comments', 'osmosis' ), __( '1 comment', 'osmosis' ), '% ' . __( 'comments', 'osmosis' ) ); ?>
		</h5>
		<ul>
		<?php wp_list_comments( 'type=comment&callback=grve_comments' ); ?>
		</ul>
	</div>
	<!-- End Comments -->

	<nav class="grve-comment-nav">
		<ul>
	  		<li><?php previous_comments_link(); ?></li>
	  		<li><?php next_comments_link(); ?></li>
		</ul>
	</nav>

<?php else : // no comments ?>

	<?php if ( comments_open() ) : ?>
    	<!-- If comments are open, but no comments. -->

	<?php else : // comments are closed ?>

	<p class="no-comments"><?php _e( 'Comments are closed.', 'osmosis' ); ?></p>

	<?php endif; ?>

<?php endif; ?>


<?php if ( comments_open() ) : ?>

<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );

		$args = array(
			'id_form'           => 'commentform',
			'id_submit'         => 'grve-comment-submit-button',
			'title_reply'       => __( 'Leave a Reply', 'osmosis' ),
			'title_reply_to'    => __( 'Leave a Reply to', 'osmosis' ) . ' %s',
			'cancel_reply_link' => __( 'Cancel Reply', 'osmosis' ),
			'label_submit'      => __( 'Submit Comment', 'osmosis' ),

			'comment_field' =>
				'<div class="grve-form-textarea">'.
				'<textarea style="resize:none;" id="comment" name="comment" placeholder="' . __( 'Your Comment Here...', 'osmosis' ) . '" cols="45" rows="15" aria-required="true">' .
				'</textarea></div><div class="clear"></div>',

			'must_log_in' =>
				'<p class="must-log-in">' . __( 'You must be', 'osmosis' ) .
				'<a href="' .  wp_login_url( get_permalink() ) . '">' . __( 'logged in', 'osmosis' ) . '</a> ' . __( 'to post a comment.', 'osmosis' ) . '</p>',

			'logged_in_as' =>
				'<p class="logged-in-as">' .  __('Logged in as','osmosis') .
				'<a href="' . admin_url( 'profile.php' ) . '"> ' . $user_identity . '</a>. ' .
				'<a href="' . wp_logout_url( get_permalink() ) . '" title="' . __( 'Log out of this account', 'osmosis' ) . '"> ' . __( 'Log out', 'osmosis' ) . '</a></p>',

			'comment_notes_before' =>
				'<p class="comment-notes">' .
				__( 'Your email address will not be published.', 'osmosis' ) .
				'</p>',

			'comment_notes_after' => '' ,

			'fields' => apply_filters(
				'comment_form_default_fields',
				array(
					'author' =>
						'<div class="grve-form-input">' .
						'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' .
						' placeholder="' . __( 'Name', 'osmosis' ) . ' ' . ( $req ? __( '(required)', 'osmosis' ) : '' ) . '" />' .
						'</div>',

					'email' =>
						'<div class="grve-form-input">' .
						'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' .
						' placeholder="' . __( 'E-mail', 'osmosis' ) . ' ' . ( $req ? __( '(required)', 'osmosis' ) : '' ) . '" />' .
						'</div>',

					'url' =>
						'<div class="grve-form-input">' .
						'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"' .
						' placeholder="' . __( 'Website', 'osmosis' )  . '" />' .
						'</div>',
					)
				),
		);
?>


			<?php
				//Use comment_form() with no parameters if you want the default form instead.
				comment_form( $args );
			?>


<?php endif;  ?>
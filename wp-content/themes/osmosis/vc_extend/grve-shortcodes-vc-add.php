<?php
/*
 *	Greatives Visual Composer Shortcode Extentions
 *
 * 	@author		Greatives Team
 * 	@URI		http://greatives.eu
 */


if ( function_exists( 'vc_add_param' ) ) {

	//Generic css aniation for elements

	$grve_add_animation = array(
		"type" => "dropdown",
		"heading" => __("CSS Animation", 'osmosis' ),
		"param_name" => "animation",
		"admin_label" => true,
		"value" => array(
			__( "No", 'osmosis' ) => '',
			__( "Fade In", 'osmosis' ) => "fadeIn",
			__( "Fade In Up", 'osmosis' ) => "fadeInUp",
			__( "Fade In Down", 'osmosis' ) => "fadeInDown",
			__( "Fade In Left", 'osmosis' ) => "fadeInLeft",
			__( "Fade In Right", 'osmosis' ) => "fadeInRight",
		),
		"description" => __("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", 'osmosis' ),
	);

	$grve_add_animation_delay = array(
		"type" => "textfield",
		"heading" => __( 'Css Animation Delay', 'osmosis' ),
		"param_name" => "animation_delay",
		"value" => '200',
		"description" => __( "Add delay in milliseconds.", 'osmosis' ),
	);

	$grve_add_margin_bottom = array(
		"type" => "textfield",
		"heading" => __( 'Bottom margin', 'osmosis' ),
		"param_name" => "margin_bottom",
		"description" => __( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'osmosis' ),
	);

	$grve_add_el_class = array(
		"type" => "textfield",
		"heading" => __("Extra class name", 'osmosis' ),
		"param_name" => "el_class",
		"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'osmosis' ),
	);

	$grve_column_width_list = array(
		__('1 column - 1/12', 'osmosis' ) => '1/12',
		__('2 columns - 1/6', 'osmosis' ) => '1/6',
		__('3 columns - 1/4', 'osmosis' ) => '1/4',
		__('4 columns - 1/3', 'osmosis' ) => '1/3',
		__('5 columns - 5/12', 'osmosis' ) => '5/12',
		__('6 columns - 1/2', 'osmosis' ) => '1/2',
		__('7 columns - 7/12', 'osmosis' ) => '7/12',
		__('8 columns - 2/3', 'osmosis' ) => '2/3',
		__('9 columns - 3/4', 'osmosis' ) => '3/4',
		__('10 columns - 5/6', 'osmosis' ) => '5/6',
		__('11 columns - 11/12', 'osmosis' ) => '11/12',
		__('12 columns - 1/1', 'osmosis' ) => '1/1'
	);

	$grve_column_desktop_hide_list = array(
		__('Default value from width attribute', 'osmosis') => '',
		__( 'Hide', 'osmosis' ) => 'hide',
	);

	$grve_column_width_tablet_list = array(
		__('Default value from width attribute', 'osmosis') => '',
		__( 'Hide', 'osmosis' ) => 'hide',
		__( '1 column - 1/12', 'osmosis' ) => '1-12',
		__( '2 columns - 1/6', 'osmosis' ) => '1-6',
		__( '3 columns - 1/4', 'osmosis' ) => '1-4',
		__( '4 columns - 1/3', 'osmosis' ) => '1-3',
		__( '5 columns - 5/12', 'osmosis' ) => '5-12',
		__( '6 columns - 1/2', 'osmosis' ) => '1-2',
		__( '7 columns - 7/12', 'osmosis' ) => '7-12',
		__( '8 columns - 2/3', 'osmosis' ) => '2-3',
		__( '9 columns - 3/4', 'osmosis' ) => '3-4',
		__( '10 columns - 5/6', 'osmosis' ) => '5-6',
		__( '11 columns - 11/12', 'osmosis' ) => '11-12',
		__( '12 columns - 1/1', 'osmosis' ) => '1',
	);

	$grve_column_width_tablet_sm_list = array(
		__('Inherit from Tablet Landscape', 'osmosis') => '',
		__( 'Hide', 'osmosis' ) => 'hide',
		__( '1 column - 1/12', 'osmosis' ) => '1-12',
		__( '2 columns - 1/6', 'osmosis' ) => '1-6',
		__( '3 columns - 1/4', 'osmosis' ) => '1-4',
		__( '4 columns - 1/3', 'osmosis' ) => '1-3',
		__( '5 columns - 5/12', 'osmosis' ) => '5-12',
		__( '6 columns - 1/2', 'osmosis' ) => '1-2',
		__( '7 columns - 7/12', 'osmosis' ) => '7-12',
		__( '8 columns - 2/3', 'osmosis' ) => '2-3',
		__( '9 columns - 3/4', 'osmosis' ) => '3-4',
		__( '10 columns - 5/6', 'osmosis' ) => '5-6',
		__( '11 columns - 11/12', 'osmosis' ) => '11-12',
		__( '12 columns - 1/1', 'osmosis' ) => '1',
	);
	$grve_column_mobile_width_list = array(
		__( '12 columns - 1/1', 'osmosis' ) => '',
		__( 'Hide', 'osmosis' ) => 'hide',
	);

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __('Section ID', 'osmosis' ),
			"param_name" => "section_id",
			"description" => __("If you wish you can type an id to use it as bookmark.", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __('Section Title', 'osmosis' ),
			"param_name" => "section_title",
			"description" => __("If you wish you can type a title for the side dot navigation.", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => __('Font Color', 'osmosis' ),
			"param_name" => "font_color",
			"description" => __("Select font color", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Heading Color", 'osmosis' ),
			"param_name" => "heading_color",
			"value" => array(
				__( "Default", 'osmosis' ) => '',
				__( "Dark", 'osmosis' ) => 'dark',
				__( "Light", 'osmosis' ) => 'light',
				__( "Primary 1", 'osmosis' ) => 'primary-1',
				__( "Primary 2", 'osmosis' ) => 'primary-2',
				__( "Primary 3", 'osmosis' ) => 'primary-3',
				__( "Primary 4", 'osmosis' ) => 'primary-4',
				__( "Primary 5", 'osmosis' ) => 'primary-5',
			),
			"description" => __( "Select heading color", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row", $grve_add_el_class );


	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Section Type", 'osmosis' ),
			"param_name" => "section_type",
			"value" => array(
				__( "Full Width Background", 'osmosis' ) => 'fullwidth-background',
				__( "In Container", 'osmosis' ) => 'in-container',
				__( "Full Width Element", 'osmosis' ) => 'fullwidth-element',
			),
			"description" => __( "Select section type", 'osmosis' ),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Flex Column Height", 'osmosis'),
			"param_name" => "flex_height",
			"description" => __( "If selected columns will have equal height. Recommended for multiple columns with different background colors.", 'osmosis' ),
			"value" => Array(__( "Flex column height", 'osmosis' ) => 'yes'),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Section Window Height", 'osmosis' ),
			"param_name" => "section_full_height",
			"value" => array(
				__( "No", 'osmosis' ) => 'no',
				__( "Yes", 'osmosis' ) => 'yes',
			),
			"description" => __( "Select if you want your section height to be equal with the window height", 'osmosis' ),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => __( "Background Type", 'osmosis' ),
			"param_name" => "bg_type",
			"description" => __( "Select Background type", 'osmosis' ),
			"value" => array(
				__("None", 'osmosis' ) => '',
				__("Color", 'osmosis' ) => 'color',
				__("Image", 'osmosis' ) => 'image',
				__("Hosted Video", 'osmosis' ) => 'hosted_video',
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => __( "Custom Background Color", 'osmosis' ),
			"param_name" => "bg_color",
			"description" => __( "Select background color for your row", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'color' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "attach_image",
			"heading" => __('Background Image', 'osmosis' ),
			"param_name" => "bg_image",
			"description" => __("Select background image for your row. Used also as fallback for video.", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Background Image Type", 'osmosis' ),
			"param_name" => "bg_image_type",
			"value" => array(
				__("Default", 'osmosis' ) => '',
				__("Fixed", 'osmosis' ) => 'fixed-bg',
				__("Parallax", 'osmosis' ) => 'parallax',
				__("Animated", 'osmosis' ) => 'animated'
			),
			"description" => __( "Select how a background image will be displayed", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __("WebM File URL", 'osmosis'),
			"param_name" => "bg_video_webm",
			"description" => __( "Fill WebM and mp4 format for browser compatibility", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __( "MP4 File URL", 'osmosis' ),
			"param_name" => "bg_video_mp4",
			"description" => __( "Fill mp4 format URL", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __( "OGV File URL", 'osmosis' ),
			"param_name" => "bg_video_ogv",
			"description" => __( "Fill OGV format URL ( optional )", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Pattern overlay", 'osmosis'),
			"param_name" => "pattern_overlay",
			"description" => __( "If selected, a pattern will be added.", 'osmosis' ),
			"value" => Array(__( "Add pattern", 'osmosis' ) => 'yes'),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Color overlay", 'osmosis' ),
			"param_name" => "color_overlay",
			"value" => array(
				__( "None", 'osmosis' ) => '',
				__( "Dark", 'osmosis' ) => 'dark',
				__( "Light", 'osmosis' ) => 'light',
				__( "Primary 1", 'osmosis' ) => 'primary-1',
				__( "Primary 2", 'osmosis' ) => 'primary-2',
				__( "Primary 3", 'osmosis' ) => 'primary-3',
				__( "Primary 4", 'osmosis' ) => 'primary-4',
				__( "Primary 5", 'osmosis' ) => 'primary-5',
			),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video' )
			),
			"description" => __( "A color overlay for the media", 'osmosis' ),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => __( "Opacity overlay", 'osmosis' ),
			"param_name" => "opacity_overlay",
			"value" => array( 10, 20, 30 ,40, 50, 60, 70, 80 ,90 ),
			"description" => __( "Opacity of the overlay", 'osmosis' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video' )
			),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __( "Top padding", 'osmosis' ),
			"param_name" => "padding_top",
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'no' )
			),
			"description" => __( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'osmosis' ),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => __( "Bottom padding", 'osmosis' ),
			"param_name" => "padding_bottom",
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'no' )
			),
			"description" => __( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'osmosis' ),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
		"type" => "textfield",
		"heading" => __( 'Bottom margin', 'osmosis' ),
		"param_name" => "margin_bottom",
		"description" => __( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'osmosis' ),
		"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Header Section", 'osmosis' ),
			"param_name" => "header_feature",
			"description" => __( "Use this option if first section ( no gap from header )", 'osmosis' ),
			"value" => Array(__( "Header section", 'osmosis' ) => 'yes'),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Footer Section", 'osmosis' ),
			"param_name" => "footer_feature",
			"description" => __( "Use this option if last section ( no gap from footer )", 'osmosis' ),
			"value" => Array(__( "Footer section", 'osmosis' ) => 'yes'),
			"group" => __( "Section Options", 'osmosis' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Desktop Visibility", 'osmosis'),
			"param_name" => "desktop_visibility",
			"description" => __( "If selected, row will be hidden on desktops/laptops.", 'osmosis' ),
			"value" => Array(__( "Hide", 'osmosis' ) => 'hide'),
			'group' => __( "Responsiveness", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Tablet Landscape Visibility", 'osmosis'),
			"param_name" => "tablet_visibility",
			"description" => __( "If selected, row will be hidden on tablet devices with landscape orientation.", 'osmosis' ),
			"value" => Array(__( "Hide", 'osmosis' ) => 'hide'),
			'group' => __( "Responsiveness", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Tablet Portrait Visibility", 'osmosis'),
			"param_name" => "tablet_sm_visibility",
			"description" => __( "If selected, row will be hidden on tablet devices with portrait orientation.", 'osmosis' ),
			"value" => Array(__( "Hide", 'osmosis' ) => 'hide'),
			'group' => __( "Responsiveness", 'osmosis' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => __( "Mobile Visibility", 'osmosis'),
			"param_name" => "mobile_visibility",
			"description" => __( "If selected, row will be hidden on mobile devices.", 'osmosis' ),
			"value" => Array(__( "Hide", 'osmosis' ) => 'hide'),
			'group' => __( "Responsiveness", 'osmosis' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => __( "Width", 'osmosis' ),
			'param_name' => 'width',
			'value' => $grve_column_width_list,
			'group' => __( "Width & Responsiveness", 'osmosis' ),
			'description' => __( "Select column width.", 'osmosis' ),
			'std' => '1/1'
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => __( "Desktop", 'osmosis' ),
			"param_name" => "desktop_hide",
			"value" => $grve_column_desktop_hide_list,
			"description" => __( "Responsive column on desktops/laptops.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => __( "Tablet Landscape", 'osmosis' ),
			"param_name" => "tablet_width",
			"value" => $grve_column_width_tablet_list,
			"description" => __( "Responsive column on tablet devices with landscape orientation.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => __( "Tablet Portrait", 'osmosis' ),
			"param_name" => "tablet_sm_width",
			"value" => $grve_column_width_tablet_sm_list,
			"description" => __( "Responsive column on tablet devices with portrait orientation.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => __( "Mobile", 'osmosis' ),
			"param_name" => "mobile_width",
			"value" => $grve_column_mobile_width_list,
			"description" => __( "Responsive column on mobile devices.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);

	vc_add_param( "vc_column_inner",
		array(
			'type' => 'dropdown',
			'heading' => __( "Width", 'osmosis' ),
			'param_name' => 'width',
			'value' => $grve_column_width_list,
			'group' => __( "Width & Responsiveness", 'osmosis' ),
			'description' => __( "Select column width.", 'osmosis' ),
			'std' => '1/1'
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => __( "Desktop", 'osmosis' ),
			"param_name" => "desktop_hide",
			"value" => $grve_column_desktop_hide_list,
			"description" => __( "Responsive column on desktops/laptops.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => __( "Tablet Landscape", 'osmosis' ),
			"param_name" => "tablet_width",
			"value" => $grve_column_width_tablet_list,
			"description" => __( "Responsive column on tablet devices with landscape orientation.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => __( "Tablet Portrait", 'osmosis' ),
			"param_name" => "tablet_sm_width",
			"value" => $grve_column_width_tablet_sm_list,
			"description" => __( "Responsive column on tablet devices with portrait orientation.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => __( "Mobile", 'osmosis' ),
			"param_name" => "mobile_width",
			"value" => $grve_column_mobile_width_list,
			"description" => __( "Responsive column on mobile devices.", 'osmosis' ),
			'group' => __( 'Width & Responsiveness', 'osmosis' ),
		)
	);

	vc_add_param( "vc_tabs",
		array(
			"type" => "dropdown",
			"heading" => __( "Tab Type", 'osmosis' ),
			"param_name" => "tab_type",
			"value" => array(
				__( "Horizontal", 'osmosis' ) => 'horizontal',
				__( "Vertical", 'osmosis' ) => 'vertical',
			),
			"description" => __( "Select tab type", 'osmosis' ),
		)
	);

	vc_add_param( "vc_tabs", $grve_add_margin_bottom );

	vc_add_param( "vc_accordion",
		array(
			"type" => 'checkbox',
			"heading" => __( "Toggle", 'osmosis'),
			"param_name" => "toggle",
			"description" => __( "If selected, accordion will be displayed as toggle.", 'osmosis' ),
			"value" => Array(__( "Convert to toggle.", 'osmosis' ) => 'yes'),
		)
	);

	vc_add_param( "vc_accordion",
		array(
			"type" => "dropdown",
			"heading" => __( "Initial State", 'osmosis' ),
			"param_name" => "initial_state",
			"admin_label" => true,
			"value" => array(
				__( "First Open", 'osmosis' ) => 'first',
				__( "All Closed", 'osmosis' ) => 'none',
			),
			"description" => __( "Accordion Initial State", 'osmosis' ),
		)
	);

	vc_add_param( "vc_accordion", $grve_add_margin_bottom );
	vc_add_param( "vc_accordion", $grve_add_el_class );

	vc_add_param( "vc_column_text",
		array(
			"type" => "dropdown",
			"heading" => __( "Text Style", 'osmosis' ),
			"param_name" => "text_style",
			"value" => array(
				__( "None", 'osmosis' ) => '',
				__( "Leader", 'osmosis' ) => 'leader-text',
				__( "Subtitle", 'osmosis' ) => 'subtitle',
			),
			"description" => __( "Select your text style", 'osmosis' ),
		)
	);
	vc_add_param( "vc_column_text", $grve_add_animation );
	vc_add_param( "vc_column_text", $grve_add_animation_delay );

	vc_add_param( "vc_widget_sidebar",
		array(
			'type' => 'hidden',
			'param_name' => 'title',
			'value' => '',
		)
	);	
	
	if ( defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '4.6', '>=') ) {

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'tab_position',
				'value' => 'top',
			)
		);

		vc_add_param( "vc_tta_accordion",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tour",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);
	}

}


?>
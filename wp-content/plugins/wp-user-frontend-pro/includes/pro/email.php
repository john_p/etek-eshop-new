<!DOCTYPE html>
<html dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">


<div dir="ltr" style="background-color:#f5f5f5;margin:0;padding:70px 0 70px 0;width:100%">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr>
            <td align="center" valign="top">
                <div>
                </div>
                    	<span class="HOEnZb"><font color="#888888">
                            </font></span><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:3px!important">
                    <tbody><tr>
                        <td align="center" valign="top">

                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#557da1;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif"><tbody><tr>
                                    <td style="padding:36px 48px;display:block">
                                        <h1 style="color:#ffffff;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left">Update member profile</h1>
                                    </td>
                                </tr></tbody></table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                                    
                                	<span class="HOEnZb"><font color="#888888">
                                        </font></span><table border="0" cellpadding="0" cellspacing="0" width="600"><tbody><tr>
                                    <td valign="top" style="background-color:#fdfdfd">
                                                
                                                <span class="HOEnZb"><font color="#888888">
                                                    </font></span><table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr>
                                                <td valign="top" style="padding:48px">
                                                    <div style="color:#737373;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">

                                                        <table cellspacing="0" cellpadding="6" style="width:100%;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;color:#737373;border:1px solid #e4e4e4" border="1">

                                                            <tbody>
                                                            <?php foreach($email_data as $item){ ?>
                                                            <tr>
                                                                <td style="text-align:left;vertical-align:middle;border:1px solid #eee;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word;color:#737373;padding:12px">
                                                                    <?php echo $item['label']; ?>
                                                                </td>
                                                                <td style="text-align:left;vertical-align:middle;border:1px solid #eee;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;color:#737373;padding:12px">
                                                                    <?php echo $item['value']; ?>
                                                                </td>

                                                            </tr>
                                                            <?php } ?>
                                                            </tbody>

                                                        </table>
                                                        <?php if(isset($customer_detail) && count($customer_detail) > 0){ ?>
                                                        <h2 style="color:#557da1;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:16px 0 8px;text-align:left">Customer details</h2>
                                                        <?php foreach($customer_detail as $item){ ?>
                                                        <p style="margin:0 0 16px"><strong><?php echo $item['label']; ?>:</strong> <span style="color:#505050;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif"><?php echo $item['value']; ?></span></p>
                                                        <?php } ?>
                                                        <?php } ?>

                                                    </div><span class="HOEnZb"><font color="#888888">
                                                        </font></span></td></tr></tbody></table><span class="HOEnZb"><font color="#888888">

                                            </font></span></td></tr></tbody></table><span class="HOEnZb"><font color="#888888">

                                </font></span></td></tr>
                    <tr>
                        <td align="center" valign="top">

                            <table border="0" cellpadding="10" cellspacing="0" width="600"><tbody><tr>
                                    <td valign="top" style="padding:0">
                                        <table border="0" cellpadding="10" cellspacing="0" width="100%"><tbody><tr>
                                                <td colspan="2" valign="middle" style="padding:0 48px 48px 48px;border:0;color:#99b1c7;font-family:Arial;font-size:12px;line-height:125%;text-align:center">
                                                    <p>ETEK</p>
                                                </td>
                                            </tr></tbody></table>
                                    </td>
                                </tr></tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr></tbody></table><div class="yj6qo"></div><div class="adL">
    </div>
</div>

</body>
</html>
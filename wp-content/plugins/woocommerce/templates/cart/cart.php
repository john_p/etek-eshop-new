<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form id="form-cart" action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<table class="shop_table cart" cellspacing="0">
	<thead>
		<tr>
			<th class="product-remove">&nbsp;</th>
			<th class="product-thumbnail">&nbsp;</th>
			<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
            <th class="product-name"><?php _e( 'Shipping Method', 'woocommerce' ); ?></th>
			<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
			<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

					<td class="product-remove">
						<?php
							echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
								'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
								esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
								__( 'Remove this item', 'woocommerce' ),
								esc_attr( $product_id ),
								esc_attr( $_product->get_sku() )
							), $cart_item_key );
						?>
					</td>

					<td class="product-thumbnail">
						<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() ) {
								echo $thumbnail;
							} else {
								printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
							}
						?>
					</td>

					<td class="product-name">
						<?php
                            $invoce_no = (isset($cart_item['addons'][0]['value']) && $cart_item['addons'][0]['value'] != '')?' <span class="bluee">(INV.NO.:'.$cart_item['addons'][0]['value'].')</span>':'';
							if ( ! $_product->is_visible() ) {
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title().$invoce_no, $cart_item, $cart_item_key ) . '&nbsp;';
							} else {
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title().$invoce_no ), $cart_item, $cart_item_key );
							}
                            /*$wc_shipping = WC_Shipping::instance();
                            var_dump( $wc_shipping->get_shipping_methods() );*/
                            /*echo '<pre>';
                            print_r($cart_item);
                            echo '</pre>';*/
							// Meta data
							//echo WC()->cart->get_item_data( $cart_item );

							// Backorder notification
							if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
								echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
							}
						?>
					</td>

                    <td class="product-name shipping-method">
                        <?php //var_dump($cart_item['ship']); ?>
                        <?php $shippings = get_post_meta( $product_id, 'shippings'); ?>
                        <?php if(isset($shippings[0])){ ?>
                            <?php $shippings = $shippings[0]; ?>
                            <?php if ( count($shippings) > 1 ) { ?>

							<?php
								$defoult_ship = isset($cart_item['ship'])?$cart_item['ship']:0;
								//var_dump($defoult_ship);
								?>
                            <select class="ship_method" id="product_ship" name="ship_method[<?php echo $cart_item_key; ?>]" onchange="change_shipping( this );">
                                <option value='0' <?php if($defoult_ship === 0){ ?> selected="selected"<?php } ?>>Select Shipment Method</option>
                                <?php foreach($shippings as $ship){ ?>
                                    <?php $option = get_option( 'woocommerce_'.$ship.'_settings' ); ?>
                                    <option value='<?php echo $ship; ?>' <?php if($defoult_ship === $ship){ ?> selected="selected"<?php } ?>><?php echo (isset($option['title']))?$option['title']:''; ?></option>
                                <?php } ?>
                                <?php /*<option value = 'flat_rate'>Delivery to address</option>
                                <option value = 'local_delivery'>Sent via post - You will receive a receipt via post</option>
                                <option value = 'local_pickup'>Pickup from our offices</option>*/ ?>
                            </select>
                            <?php }else if(isset($shippings[0])){ ?>
                                <?php $option = get_option( 'woocommerce_'.$shippings[0].'_settings' ); ?>
                                <input  class="ship_method" type="hidden" name="ship_method[<?php echo $cart_item_key; ?>]" value="<?php echo $shippings[0]; ?>"/>
                                <span><?php echo (isset($option['title']))?$option['title']:''; ?></span>
                            <?php } ?>
                        <?php } ?>
                    </td>

					<td class="product-price">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?>
					</td>

					<td class="product-quantity">
						<?php
							if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
									'min_value'   => '0'
								), $_product, false );
							}

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
						?>
					</td>

					<td class="product-subtotal">
						<?php
							echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</td>
				</tr>
				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>
        <?php /*
		<tr>
			<td colspan="7" class="actions">

				<?php if ( WC()->cart->coupons_enabled() ) { ?>
					<div class="coupon">

						<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

						<?php do_action( 'woocommerce_cart_coupon' ); ?>
					</div>
				<?php } ?>

				<input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />

				<?php do_action( 'woocommerce_cart_actions' ); ?>

				<?php wp_nonce_field( 'woocommerce-cart' ); ?>
			</td>
		</tr>*/ ?>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>
    <div class="cart_btn_update">
        <input type="submit" class="button pull-right" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />

        <?php do_action( 'woocommerce_cart_actions' ); ?>

        <?php wp_nonce_field( 'woocommerce-cart' ); ?>
    </div>
<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>
<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery(document).on('click', 'a.checkout-button', function(){
			jQuery('.woocommerce-error').remove();

			var error = true;
			jQuery('select#product_ship').each(function(){
				if(jQuery(this).val() == 0){
					error = false;
				}
				
			});

			if(error){

				return error;

			}else{
				//console.log('send messages');
				var ul = jQuery('<ul />');
                ul.addClass('woocommerce-error');
				
				var li = jQuery('<li />');
                li.html('Select Shipment Method');
                ul.prepend(li);
				
				jQuery('form#form-cart').prepend(ul);
				return false;
			}

        });



	});

    function change_shipping( obj ){
        jQuery( '.woocommerce' ).block({
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            }
        });
        var data = jQuery('.ship_method').serialize() + '&action=load_woo_cart';

        jQuery.get( woocommerce_params.ajax_url, data, function( response ) {
            response = JSON.parse( response );
            if(response.total){
                jQuery('#for_ajax').html(response.total);
            }

            if(response.remove_err){
                jQuery('.woocommerce-error').remove();
            }

            jQuery( '.woocommerce' ).unblock();
        });

    }
</script>
<?php do_action( 'woocommerce_after_cart' ); ?>

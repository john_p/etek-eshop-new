<table class="shop_table shop_table_responsive customer_details tab_second">

    <tbody>
        <tr>
            <th>Address</th>
            <td id="tab_adddress<?php echo $checkout->key; ?>"><?php echo $checkout->get_value( 'billing_address_1' ); ?></td>
        </tr>

        <tr>
            <th>City</th>
            <td id="tab_city<?php echo $checkout->key; ?>"><?php echo $checkout->get_value( 'billing_city' ); ?></td>
        </tr>

        <tr>
            <th>Country</th>
            <td id="tab_country<?php echo $checkout->key; ?>"><?php echo WC()->countries->countries[ $checkout->get_value( 'billing_country' ) ]; ?></td>
        </tr>

        <tr>
            <th>Postal Code</th>
            <td id="tab_postcode<?php echo $checkout->key; ?>"><?php echo $checkout->get_value( 'billing_postcode' ); ?></td>
        </tr>

        <tr>
            <th>Telephone</th>
            <td id="tab_telephone<?php echo $checkout->key; ?>"><?php echo $checkout->get_value( 'billing_phone' ); ?></td>
        </tr>
    </tbody>
</table>
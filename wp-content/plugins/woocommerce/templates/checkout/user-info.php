<h2>Profile Details</h2>
<?php
global $current_user;
get_currentuserinfo();
?>
<table class="shop_table shop_table_responsive customer_details tab_first">
    <tbody>

    <?php //if($current_user->user_firstname){ ?>
        <tr>
            <th>First Name</th>
            <td id="tab_billing_first_name"><?php echo $current_user->user_firstname ? $current_user->user_firstname : ''; ?></td>
        </tr>
    <?php //} ?>
    <?php //if($current_user->user_lastname){ ?>
        <tr>
            <th>Surname</th>
            <td id="tab_billing_last_name"><?php echo $current_user->user_lastname ? $current_user->user_lastname : ''; ?></td>
        </tr>
    <?php //} ?>
    <?php //if($current_user->type){ ?>
        <tr>
            <th>Type</th>
            <td id="tab_type"><?php echo $current_user->type ? $current_user->type : ''; ?></td>
        </tr>
    <?php //} ?>
    <?php //if($current_user->etek_no_){ ?>
        <tr>
            <th>ETEK NO.</th>
            <td id="tab_etek_no_"><?php echo $current_user->etek_no_ ? $current_user->etek_no_: ''; ?></td>
        </tr>
    <?php //} ?>
    <?php //if($current_user->user_email){ ?>
        <tr>
            <th>Email</th>
            <td id="tab_billing_email"><?php echo $current_user->user_email ? $current_user->user_email : ''; ?></td>
        </tr>
    <?php //} ?>
    </tbody>
</table>
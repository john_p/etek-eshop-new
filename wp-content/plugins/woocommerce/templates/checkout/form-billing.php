<?php
/**
 * Checkout billing information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */
?>
<div class="woocommerce-billing-fields">

	<?php if(!is_user_logged_in()){ ?>
		<h2><?php _e( 'Profile Details', 'woocommerce' ); ?></h2>
	<?php }else{ ?>
		<h2><?php _e( 'Select Address', 'woocommerce' ); ?><abbr title="required" class="required" style="color:red;">*</abbr></h2>


	<?php } ?>



	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
    <?php //if (  !is_user_logged_in() ) { ?>
        <?php //echo get_class($checkout); ?>
        <?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>
            <?php //echo $key .' - ' ?>
			<?php //echo $checkout->get_value( $key ).'<br />'; ?>
            <?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php  if(is_user_logged_in() && $key === 'billing_alt'){ ?>
				<?php do_action( 'woocommerce_checkout_address_info' ); ?>
				<a href="/member-profile/multiple-shipping-addresses/" class="btn_address_edit">Click here to edit your addresses</a>
			<?php } ?>
        <?php endforeach; ?>
        <?php /*<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Order Confirmation" data-value="Order Confirmation">*/ ?>
    <?php /*}else{ ?>
    <?php }*/ ?>


	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<p class="form-row form-row-wide create-account">
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>

</div>

<div class="wrapp-checkbox-add-address">
    <p>
        <label>Add new address?</label>
        <input type="checkbox" name="btn_add_address" id="btn_add_address"/>
    </p>
    <div class="add_new_address_wrapper">
        <p class="form-row form-row form-row-first validate-required">
            <label for="new_address_name">
                Address Name
                <abbr title="required" class="required">*</abbr>
            </label>
            <input name="new_address_name" id="new_address_name" type="text" value="" class="input-text"/>
        </p>

        <p class="form-row form-row form-row-wide validate-required">
            <label for="new_address">
                Address
                <abbr title="required" class="required">*</abbr>
            </label>
            <input name="new_address" id="new_address" type="text" value="" class="input-text"/>
        </p>

        <p class="form-row form-row form-row-first validate-required">
            <label for="new_city">
                City
                <abbr title="required" class="required">*</abbr>
            </label>
            <input name="new_city" id="new_city" type="text" value="" class="input-text"/>
        </p>

        <p class="form-row form-row form-row-last validate-required">
            <label for="new_postalcode">
                Postal Code
                <abbr title="required" class="required">*</abbr>
            </label>
            <input name="new_postalcode" id="new_postalcode" type="text" value="" class="input-text"/>
        </p>

        <p class="form-row form-row form-row-first validate-required">
            <label for="new_telephone">
                Telephone
                <abbr title="required" class="required">*</abbr>
            </label>
            <input name="new_telephone" id="new_telephone" type="text" value="" class="input-text"/>
        </p>

        <p class="form-row form-row form-row-last validate-required">
            <label for="new_country">
                Country
            </label>
            <?php echo WC()->countries->countries[ $checkout->get_value( 'billing_country' ) ]; ?>
            <input name="new_country" id="new_country" type="hidden" value="<?php echo $checkout->get_value( 'billing_country' ); ?>" class="input-text"/>
        </p>
    </div>
</div>
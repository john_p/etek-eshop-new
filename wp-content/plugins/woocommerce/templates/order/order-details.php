<?php
/**
 * Order details
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$order = wc_get_order( $order_id );
?>

<?php //wc_get_template( 'order/order-details-customer.php', array( 'order' =>  $order ) ); ?>

<div class="col2-set view-order-detail">
	<div class="col-1"><?php wc_get_template( 'order/order-details-customer.php', array( 'order' =>  $order ) ); ?></div>
	<div class="col-2">
        <div id="order_notes_box" style="margin-top: -7px;">
            <?php if(isset($order->customer_note) && $order->customer_note != ''){ ?>
                <label class="" for="order_comments">Order Notes</label>
                <p><?php echo $order->customer_note; ?></p>
            <?php } ?>
        </div>
    </div>
</div>

<h2><?php _e( 'Order Summary', 'woocommerce' ); ?></h2>
<table class="shop_table order_details tab_three">
	<thead>
		<tr>
			<th class="product-name"><?php _e( 'Item', 'woocommerce' ); ?></th>
			<th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach( $order->get_items() as $item_id => $item ) {
				wc_get_template( 'order/order-details-item.php', array(
					'order'   => $order,
					'item_id' => $item_id,
					'item'    => $item,
					'product' => apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item )
				) );
			}
		?>
		<?php do_action( 'woocommerce_order_items_table', $order ); ?>
	</tbody>
	<tfoot>
		<?php
            $ex = array(
                'payment_method',
                'vat-1',
            );
			foreach ( $order->get_order_item_totals() as $key => $total ) {
                //var_dump($total);
                if(!in_array($key, $ex)) {
                    ?>
                    <tr>
                        <th scope="row" class="table_bold"><?php echo str_replace(':', '',$total['label']); ?></th>
                        <td<?php if($key == 'order_total'){ ?> class="order_total"<?php } ?>><?php echo $total['value']; ?></td>
                    </tr>
                <?php
                }
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>



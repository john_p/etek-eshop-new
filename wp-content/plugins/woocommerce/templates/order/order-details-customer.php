<?php
/**
 * Order Customer Details
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<header><h2><?php _e( 'Profile Details', 'woocommerce' ); ?></h2></header>

<table class="shop_table shop_table_responsive customer_details tab_first">

    <?php
        global $current_user;
        get_currentuserinfo();
    //echo $order->id;
    //var_dump( get_post_meta( $order->id, 'type', true ) );
    //var_dump($order);die;
    ?>
    <?php if ( $order->billing_first_name ) { ?>
        <tr>
            <th><?php _e( 'First Name', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $order->billing_first_name ); ?></td>
        </tr>
    <?php }else if ( $current_user->user_firstname ) { ?>
        <tr>
            <th><?php _e( 'First Name', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $current_user->user_firstname ); ?></td>
        </tr>
    <?php } ?>

    <?php if ( $order->billing_last_name ) { ?>
        <tr>
            <th><?php _e( 'Surname', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $order->billing_last_name ); ?></td>
        </tr>
    <?php }else if ( $current_user->user_lastname ) { ?>
        <tr>
            <th><?php _e( 'Surname', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $current_user->user_lastname ); ?></td>
        </tr>
    <?php } ?>

    <?php if ( get_post_meta( $order->id, 'type', true ) ) { ?>
        <tr>
            <th><?php _e( 'Type', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta( $order->id, 'type', true ) ); ?></td>
        </tr>
    <?php }else if ( $current_user->type ) { ?>
        <tr>
            <th><?php _e( 'Type', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $current_user->type ); ?></td>
        </tr>
    <?php } ?>

    <?php if ( get_post_meta( $order->id, 'etek_no_', true ) ) { ?>
        <tr>
            <th><?php _e( 'ETEK NO.', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta( $order->id, 'etek_no_', true ) ); ?></td>
        </tr>
    <?php }else if ( $current_user->etek_no_ ) { ?>
        <tr>
            <th><?php _e( 'ETEK NO.', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( $current_user->etek_no_ ); ?></td>
        </tr>
    <?php } ?>

	<?php /*if ( $order->customer_note ) : ?>
		<tr>
			<th><?php _e( 'Note', 'woocommerce' ); ?></th>
			<td><?php echo wptexturize( $order->customer_note ); ?></td>
		</tr>
	<?php endif;*/ ?>

	<?php if ( $order->billing_email ) : ?>
		<tr>
			<th><?php _e( 'Email', 'woocommerce' ); ?></th>
			<td><?php echo esc_html( $order->billing_email ); ?></td>
		</tr>
	<?php endif; ?>

	<?php /*if ( $order->billing_phone ) : ?>
		<tr>
			<th><?php _e( 'Telephone:', 'woocommerce' ); ?></th>
			<td><?php echo esc_html( $order->billing_phone ); ?></td>
		</tr>
	<?php endif;*/ ?>

	<?php do_action( 'woocommerce_order_details_after_customer_details', $order ); ?>
</table>

<?php /*if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() ) : ?>

<div class="col2-set addresses">
	<div class="col-1">

<?php endif; */?>

<header class="title">
	<h2><?php _e( 'Address', 'woocommerce' ); ?></h2>
</header>
<?php /*<address>
	<?php echo ( $address = $order->get_formatted_billing_address() ) ? $address : __( 'N/A', 'woocommerce' ); ?>
</address>*/ ?>

<table class="shop_table shop_table_responsive customer_details tab_second">

    <?php if ( get_post_meta($order->id,'_billing_address_1',true) ) : ?>
        <tr>
            <th><?php _e( 'Address', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta($order->id,'_billing_address_1',true) ); ?></td>
        </tr>
    <?php endif; ?>

    <?php if ( get_post_meta($order->id,'_billing_city',true) ) : ?>
        <tr>
            <th><?php _e( 'City', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta($order->id,'_billing_city',true) ); ?></td>
        </tr>
    <?php endif; ?>

    <?php if ( get_post_meta($order->id,'_billing_country',true) ) : ?>
        <tr>
            <th><?php _e( 'Country', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( WC()->countries->countries[ get_post_meta($order->id,'_billing_country',true) ] ); ?></td>
        </tr>
    <?php endif; ?>

    <?php if ( get_post_meta($order->id,'_billing_postcode',true) ) : ?>
        <tr>
            <th><?php _e( 'Postal Code', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta($order->id,'_billing_postcode',true) ); ?></td>
        </tr>
    <?php endif; ?>

    <?php if ( get_post_meta($order->id,'_billing_phone',true) ) : ?>
        <tr>
            <th><?php _e( 'Telephone', 'woocommerce' ); ?></th>
            <td><?php echo wptexturize( get_post_meta($order->id,'_billing_phone',true) ); ?></td>
        </tr>
    <?php endif; ?>

</table>



<?php
if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() ) : ?>

	<?php /*</div><!-- /.col-1 -->
	<div class="col-2">*/ ?>
		<?php /*<header class="title">
			<h2><?php _e( 'Shipping Address', 'woocommerce' ); ?></h2>
		</header>
        <table class="shop_table shop_table_responsive customer_details tab_second">

            <?php if ( get_post_meta($order->id,'_shipping_address_1',true) ) : ?>
                <tr>
                    <th><?php _e( 'Address', 'woocommerce' ); ?></th>
                    <td><?php echo wptexturize( get_post_meta($order->id,'_shipping_address_1',true) ); ?></td>
                </tr>
            <?php endif; ?>

            <?php if ( get_post_meta($order->id,'_shipping_city',true) ) : ?>
                <tr>
                    <th><?php _e( 'City', 'woocommerce' ); ?></th>
                    <td><?php echo wptexturize( get_post_meta($order->id,'_shipping_city',true) ); ?></td>
                </tr>
            <?php endif; ?>

            <?php if ( get_post_meta($order->id,'_shipping_country',true) ) : ?>
                <tr>
                    <th><?php _e( 'Country', 'woocommerce' ); ?></th>
                    <td><?php echo wptexturize( WC()->countries->countries[ get_post_meta($order->id,'_shipping_country',true) ] ); ?></td>
                </tr>
            <?php endif; ?>

            <?php if ( get_post_meta($order->id,'_shipping_postcode',true) ) : ?>
                <tr>
                    <th><?php _e( 'Postal Code', 'woocommerce' ); ?></th>
                    <td><?php echo wptexturize( get_post_meta($order->id,'_shipping_postcode',true) ); ?></td>
                </tr>
            <?php endif; ?>

            <?php if ( get_post_meta($order->id,'_shipping_phone',true) ) : ?>
                <tr>
                    <th><?php _e( 'Telephone', 'woocommerce' ); ?></th>
                    <td><?php echo wptexturize( get_post_meta($order->id,'shipping_phone',true) ); ?></td>
                </tr>
            <?php endif; ?>

        </table>*/ ?>

		<?php /*<address>
			<?php echo ( $address = $order->get_formatted_shipping_address() ) ? $address : __( 'N/A', 'woocommerce' ); ?>
		</address>
	</div><!-- /.col-2 -->
</div><!-- /.col2-set -->*/ ?>

<?php endif; ?>

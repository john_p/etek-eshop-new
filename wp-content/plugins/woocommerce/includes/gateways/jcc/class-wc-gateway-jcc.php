<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * JCC Standard Payment Gateway
 *
 * Provides a JCC Standard Payment Gateway.
 *
 * @class 		WC_Gateway_JCC
 * @extends		WC_Payment_JCC
 * @version		2.3.0
 * @package		WooCommerce/Classes/Payment
 * @author 		shvedas
 */

//add_action('plugins_loaded', 'woocommerce_jcc_payment_init', 0);
//function woocommerce_jcc_payment_init()
//{
    if(!class_exists('WC_Gateway_JCC')) return;

    class WC_Gateway_JCC extends WC_Payment_Gateway
    {
        private $testmode = true;
        private $config = array();


        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {
            $this->id = 'jcc';
            $this->icon = apply_filters('woocommerce_jcc_icon', '');
            $this->method_title = __('JCC Payment', 'woocommerce');
            $this->method_description = __('JCC Payment', 'woocommerce');
            $this->has_fields = false;
            $this->supports           = array(
                'products',
                'refunds'
            );

            // Load the settings
            $this->init_form_fields();
            $this->init_settings();

            //var_dump($this->get_option( 'testmode' ));
            $this->testmode = 0 == $this->get_option( 'testmode' );
            //var_dump($this->testmode);
            $this->config = array(
                'merchantID'    => $this->get_option( 'merchantID' ),
                'acquiredID'    => $this->get_option( 'acquiredID' ),
                'password'      => $this->get_option( 'password' ),
                'currency'      => $this->get_option( 'currency' )
            );

            // Get settings
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->instructions = $this->get_option('instructions', $this->description);
            $this->enable_for_methods = $this->get_option('enable_for_methods', array());
            $this->enable_for_virtual = $this->get_option('enable_for_virtual', 'yes') === 'yes' ? true : false;

            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_thankyou_jcc', array($this, 'thankyou_page'));

            // Customer Emails
            //add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        }

        /**
         * Initialise Gateway Settings Form Fields
         */
        public function init_form_fields()
        {
            $shipping_methods = array();

            if (is_admin())
                foreach (WC()->shipping()->load_shipping_methods() as $method) {
                    $shipping_methods[$method->id] = $method->get_title();
                }

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable JCC', 'woocommerce'),
                    'label' => __('Enable JCC Payment', 'woocommerce'),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'title' => array(
                    'title' => __('Title', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Payment method description that the customer will see on your checkout.', 'woocommerce'),
                    'default' => __('JCC Secure Payment Gateway', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Description', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('Payment method description that the customer will see on your website.', 'woocommerce'),
                    'default' => __('JCC Secure Payment Gateway', 'woocommerce'),
                    'desc_tip' => true,
                ),


                'testmode' => array(
                    'title'             => __( 'Mode', 'woocommerce' ),
                    'type'              => 'select',
                    'default'           => 'Sandbox',
                    'description'       => __( 'Use test account or Real Payments', 'woocommerce' ),
                    'options'           => array(
                        'Sandbox', 'Real Payments'
                    ),
                    'desc_tip'          => true,

                ),

                'merchantID' => array(
                    'title' => __('Merchant ID', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                    'default' => __('0016385015', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'acquiredID' => array(
                    'title' => __('Acquired ID', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                    'default' => __('402971', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'password' => array(
                    'title' => __('Password', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                    'default' => __('qcu0Rv15', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'currency' => array(
                    'title' => __('Currency', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                    'default' => __('', 'woocommerce'),
                    'desc_tip' => true,
                ),


            );
        }

        /**
         * Check If The Gateway Is Available For Use
         *
         * @return bool
         */
        public function is_available()
        {
            $order = null;
            $needs_shipping = false;

            // Test if shipping is needed first
            if (WC()->cart && WC()->cart->needs_shipping()) {
                $needs_shipping = true;
            } elseif (is_page(wc_get_page_id('checkout')) && 0 < get_query_var('order-pay')) {
                $order_id = absint(get_query_var('order-pay'));
                $order = wc_get_order($order_id);

                // Test if order needs shipping.
                if (0 < sizeof($order->get_items())) {
                    foreach ($order->get_items() as $item) {
                        $_product = $order->get_product_from_item($item);
                        if ($_product && $_product->needs_shipping()) {
                            $needs_shipping = true;
                            break;
                        }
                    }
                }
            }

            $needs_shipping = apply_filters('woocommerce_cart_needs_shipping', $needs_shipping);

            // Virtual order, with virtual disabled
            if (!$this->enable_for_virtual && !$needs_shipping) {
                return false;
            }

            // Check methods
            if (!empty($this->enable_for_methods) && $needs_shipping) {

                // Only apply if all packages are being shipped via chosen methods, or order is virtual
                $chosen_shipping_methods_session = WC()->session->get('chosen_shipping_methods');

                if (isset($chosen_shipping_methods_session)) {
                    $chosen_shipping_methods = array_unique($chosen_shipping_methods_session);
                } else {
                    $chosen_shipping_methods = array();
                }

                $check_method = false;

                if (is_object($order)) {
                    if ($order->shipping_method) {
                        $check_method = $order->shipping_method;
                    }

                } elseif (empty($chosen_shipping_methods) || sizeof($chosen_shipping_methods) > 1) {
                    $check_method = false;
                } elseif (sizeof($chosen_shipping_methods) == 1) {
                    $check_method = $chosen_shipping_methods[0];
                }

                if (!$check_method) {
                    return false;
                }

                $found = false;

                foreach ($this->enable_for_methods as $method_id) {
                    if (strpos($check_method, $method_id) === 0) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    return false;
                }
            }

            return parent::is_available();
        }


        /**
         * Process the payment and return the result
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment($order_id)
        {

            $order = wc_get_order($order_id);

            // Mark as processing (payment won't be taken until delivery)
            //$order->update_status('processing', __('Payment to be made upon delivery.', 'woocommerce'));

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();
            include_once( 'includes/class-wc-gateway-jcc-request.php' );
            $jcc_request = new WC_Gateway_JCC_Request( $this, $this->config );
            //var_dump($this->testmode);die;
            // Return thankyou redirect
            return array(
                'result' => 'success',
                'jcc_form' => $jcc_request->get_jcc_form($order, $this->testmode)
            );
        }

        public function return_handler() {
            echo 'return_handler';die;
        }

        public function process_refund( $order_id, $amount = null, $reason = '' ) {
            echo 'process_refund';die;
        }

        /**
         * Output for the order received page.
         */
        public function thankyou_page()
        {
            if ($this->instructions) {
                echo wpautop(wptexturize($this->instructions));
            }
        }

        public function get_icon() {
            $icon  = '<img style="margin-left:20px;" src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/jcc_icon.png' ) . '" alt="JCC" />';
            //$icon  = '<img width="240" style="margin-left:20px;" src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/jcc_icon.png' ) . '" alt="Visa" />';

            //$icon  = '<img src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/visa.png' ) . '" alt="Visa" />';
            //$icon .= '<img src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/mastercard.png' ) . '" alt="MasterCard" />';
            //$icon .= '<img src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/discover.png' ) . '" alt="Discover" />';
            //$icon .= '<img src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/amex.png' ) . '" alt="Amex" />';
            //$icon .= '<img src="' . WC_HTTPS::force_https_url( WC()->plugin_url() . '/assets/images/icons/credit-cards/jcb.png' ) . '" alt="JCB" />';

            return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
        }

    }





    /**
     * Add the JCC to WooCommerce
     **/
    /*function woocommerce_add_jcc_gateway($methods) {
        $methods[] = 'WC_Gateway_JCC';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_add_jcc_gateway' );*/

//}
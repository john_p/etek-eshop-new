<?php
  class JCCGateway 
  {  
    private $merchantID;
    private $acquiredID;
    private $password;
    private $currency;

    private static $CURRENCY_ISO = 978; // Euro, 826 for Pound sterling, 840 for US Dollar
    
    public function __construct($merchantID, $acquiredID, $password, $currency = null) {

      $this->setMerchantID($merchantID);
      $this->setAcquiredID($acquiredID);
      $this->setPassword($password);
      $this->setCurrency($currency);      
    }
    
    public static function getInstance($config) {
        return new JCCGateway($config['merchantID'], $config['acquiredID'], $config['password'], $config['currency']);
    }
    
    public function setMerchantID($merchantID) {
      $this->merchantID = $merchantID;
    }
    
    public function getMerchantID() {
      return $this->merchantID;
    }
    
    public function setAcquiredID($acquiredID) {
      $this->acquiredID = $acquiredID;
    }
    
    public function getAcquiredID() {
      return $this->acquiredID;
    }
    
    public function setPassword($password) {
      $this->password = $password;
    }
    
    public function getPassword() {
      return $this->password;
    }
    
    public function setCurrency($currency) {
      $currency = (int)$currency;
      if ($currency > 0 && $currency < 1000) $this->currency = $currency;
      else $this->currency = JCCGateway::$CURRENCY_ISO; 
    }
    
    public function getCurrency() {
      return $this->currency;
    }
    
    public function getRequestSignature($orderID, $amount, $hashed = false) {
      $amount = JCCGateway::getNormalizedAmount($amount);
      $currency = JCCGateway::getNormalizedCurrency($this->currency);
      $signature = $this->password . $this->merchantID . $this->acquiredID . 
        $orderID . $amount . $currency;
      if ($hashed) return base64_encode(sha1($signature, true));
      return $signature;      
    }
    
    public function getHashedRequestSignature($orderID, $amount) {
      return $this->getRequestSignature($orderID, $amount, true);
    }
    
    public function getResponseSignature($orderID, $hashed = false) {
      $signature = $this->password . $this->merchantID . $this->acquiredID . $orderID;
      if ($hashed) return sha1($signature);
      return $signature;
    }
    
    public function getHashedResponseSignature($orderID) {
      return $this->getResponseSignature($orderID, true);
    }
    
    public function getResponseSuccess($responseSignature, $orderID, $hashed = true) {
      $expectedResponse = $this->getResponseSignature($orderID, $hashed);
      return (strcmp($responseSignature, $expectedResponse) == 0); 
    }
    
    public static function getNormalizedAmount($amount) {
      $normalizedAmount = null;
      if (is_numeric($amount)) {
        $amount = number_format((float)$amount, 2, ".", "");
        $whole = substr($amount, 0, strpos($amount, "."));
        $decimal = substr($amount, -2);
        $wholeLength = strlen($whole);         
        if ($wholeLength > 10) $whole = substr($whole, 0, 10);
        if ($wholeLength < 10) $whole = substr(("000000000" . $whole), -10);
        $normalizedAmount = $whole . $decimal;
      }
      return $normalizedAmount;
    }
    
    public static function getNormalizedCurrency($currency) {
      $normalizedCurrency = (int)$currency;
      if ($normalizedCurrency > 0)
        $normalizedCurrency = substr(("000" . $normalizedCurrency), -3);
      else $normalizedCurrency = JCCGateway::$CURRENCY_ISO;
      return $normalizedCurrency;             
    }
    
    public function __toString() {
      return get_class($this) . "(" . $this->merchantID . ":" . $this->acquiredID . ")";
    }
    
    public function __sleep() {
      return array($this->$merchantID, $this->$acquiredID, $this->$password, $this->$currency);
    }
  }
?>
<?php

class WC_Gateway_JCC_Request {

    private $url;
    protected $gateway;
    protected $config;

    public function __construct( $gateway, $config ) {
        $this->gateway    = $gateway;
        $this->config       = $config;
    }

    public function get_url($testmode){
        if( $testmode ){
            return $this->url = 'https://tjccpg.jccsecure.com/EcomPayment/RedirectAuthLink';
        }else{
            return $this->url = 'https://jccpg.jccsecure.com/EcomPayment/RedirectAuthLink';
        }
    }
    protected function get_jcc_args( $order ) {
        include_once( 'class.jcc.php' );
        $jcc = JCCGateway::getInstance($this->config);

        $response_url = 'https://etek-eshop.7demo.org.ua/checkout';
        $order_total = 10;
        $order_id = 55;


        return array(
            'Version'                       => '1.0.0',
            'MerID'                         => $jcc->getMerchantID(),
            'MerRespURL'                    => $response_url,
            'AcqID'                         => $jcc->getAcquiredID(),
            'PurchaseAmt'                   => JCCGateway::getNormalizedAmount($order_total),
            'PurchaseCurrency'              => 978,
            'PurchaseCurrencyExponent'      => 2,
            'OrderID'                       => $order_id,
            'Signature'                     => $jcc->getHashedRequestSignature($order_id, $order_total),
            'SignatureMethod'               => 'SHA1',
            'CaptureFlag'                   => 'M'
        );
    }

    public function get_jcc_form($order, $testmode){

        include_once( 'class.jcc.php' );
        $jcc = JCCGateway::getInstance($this->config);

        $response_url = site_url( 'jcc_response' ).'.php';

        //$response_url = esc_url( $order->get_cancel_order_url() );
        //$response_url = str_replace('http', 'https', esc_url( $order->get_cancel_order_url() ) );
        //$response_url = str_replace('httpss', 'https', $response_url );

        //$response_url = esc_url( add_query_arg( 'utm_nooverride', '1', $this->gateway->get_return_url( $order ) ) );
        $order_total = $order->get_total();
        //var_dump($order);die;
        $order_id = $order->id;

        return "
        <form id='jccformpayment' action='" . $this->get_url($testmode) . "' method='post'>
            <input name='Version' type='hidden' value='1.0.0'/>
            <input name='MerID' type='hidden' value='" . $jcc->getMerchantID() . "'/>
            <input name='MerRespURL' type='hidden' value='" . $response_url . "'/>
            <input name='AcqID' type='hidden' value='" . $jcc->getAcquiredID() . "'/>
            <input name='PurchaseAmt' type='hidden' class='total_price_with_tax_' value='" . JCCGateway::getNormalizedAmount($order_total) . "'/>
            <input name='PurchaseCurrency' type='hidden' value='978'/>
            <input name='PurchaseCurrencyExponent' type='hidden' value='2'/>
            <input name='OrderID' type='hidden' value='" . $order_id  . "'/>
            <input name='Signature' class='Signature_' type='hidden' value='" . $jcc->getHashedRequestSignature($order_id, $order_total) . "'/>
            <input name='SignatureMethod' type='hidden' value='SHA1'/>
            <input name='CaptureFlag' type='hidden' value='M'/>
        </form>

      ";
    }


    public function get_request_url( $order, $testmode = false ) {

        return $this->get_url($testmode). '?' .http_build_query( $this->get_jcc_args($order), '', '&' );

    }

}
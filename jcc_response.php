<?php

session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/wp-config.php' );
global $wpdb;

if(isset($_POST["ResponseCode"])){
    $response = (int)$_POST["ResponseCode"];
    $response_code_desc=$_POST["ReasonCodeDesc"];
    $orderID = $_POST["OrderID"];
    $order          = wc_get_order( $orderID );
    $signature = $_POST["Signature"];
    $url = esc_url( get_return_url( $order ) ).'&utm_nooverride=1';
    if($response==1 && $response_code_desc=="Approved"){
        //echo 'Approve';
        //$order->update_status( 'processing' );
        $order->payment_complete();
        //WC()->mailer();
        do_action('send_multiples_emails_per_product', $orderID);
        //$url = esc_url( add_query_arg( 'utm_nooverride', '1', get_return_url( $order ) ));
    }else{
        //echo 'Error';
        $order->update_status( 'failed' );
        $url .= "&shippay=declined";
        //$url = esc_url( $order->get_cancel_order_url() );
        //var_dump($signature);
        //wc_add_notice( 'Sorry, your payment failed. No charges were made.', 'error' );
    }

    //var_dump($url);
    wp_redirect($url, 301);
    exit();
}


function get_return_url( $order = null ) {

    if ( $order ) {
        $return_url = $order->get_checkout_order_received_url();
    } else {
        $return_url = wc_get_endpoint_url( 'order-received', '', wc_get_page_permalink( 'checkout' ) );
    }

    if ( is_ssl() || get_option('woocommerce_force_ssl_checkout') == 'yes' ) {
        $return_url = str_replace( 'http:', 'https:', $return_url );
    }

    return apply_filters( 'woocommerce_get_return_url', $return_url, $order );
}